<?php require 'api/core.php'; ?>
<?php date_default_timezone_set('America/Sao_Paulo'); ?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="author" content="Layout - http://layoutnet.com.br">
    <meta name=”description” content="A Anabelle Cosméticos celebra a beleza com uma linha completa de produtos profissionais para cabelos, levando inovação e modernização aos salões de beleza do Brasil.">
    <title>Anabelle Cosméticos</title>
    <base href="<?php echo Request::getBaseUrl(); ?>">

    <!-- Favicons iOS, Android, Browser -->
    <link rel="apple-touch-icon" sizes="57x57" href="assets/image/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="assets/image/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="assets/image/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="assets/image/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="assets/image/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="assets/image/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="assets/image/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="assets/image/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="assets/image/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="assets/image/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/image/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="assets/image/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/image/favicon-16x16.png">
    <link rel="manifest" href="assets/image/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="assets/image/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">


    <!-- OGtitle -->
    <meta property="og:title" content="Anabelle Cosméticos"/>
    <meta property="og:description" content="A Anabelle Cosméticos celebra a beleza com uma linha completa de produtos profissionais para cabelos, levando inovação e modernização aos salões de beleza do Brasil.">
    <meta property="og:type" content="website">
    <meta property="og:image" content="assets/img/logo-header.png">

    <!-- CSS -->
    <link rel="stylesheet" type="text/css" href="assets/css/main.css">

    <!-- CSS Owl Carousel -->
    <link rel="stylesheet" href="assets/libs/owlcarousel/assets/owl.theme.default.css">
    <link rel="stylesheet" href="assets/libs/owlcarousel/assets/owl.carousel.css">

    <!-- jQuery -->
    <script src="assets/js/jquery.min.js"></script>
</head>

<body>

    <!-- GET de Arquivos  -->
    <?php
    extract($_GET);
    require '_header.php';
    require 'pages/' . $page . '.php';
    require '_footer.php';
    ?>



    <!-- Script OwlCarousel -->
    <script>
        $(document).ready(function() {
            $('.owl-carousel').owlCarousel({
                margin: 10,
                nav: true,
                autoplay: false,
                loop: true,
                dots: false,
                navText: ["<img src='assets/image/left-sign.png'>",
                    "<img src='assets/image/right-sign.png'>"
                ],
                responsive: {
                    0: {
                        items: 1
                    },
                    600: {
                        items: 3
                    },
                    1000: {
                        items: 4
                    }
                }
            });
        })
    </script>



    <!-- JS -->
    <script src="assets/js/uikit.min.js"></script>
    <script src="assets/js/uikit-icons.min.js"></script>
    <script src="assets/js/formulario.js"></script>
    <script src="assets/js/counter.js"></script>

    <!-- JS OwlCarousel -->
    <script type="text/javascript" src="assets/libs/owlcarousel/owl.carousel.min.js"></script>
</body>

</html>
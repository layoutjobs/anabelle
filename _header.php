<header>
    <div class="uk-container uk-container-large">
        <nav class="uk-navbar-container" uk-navbar>
            <div class="uk-navbar-left">
                <a class="uk-navbar-item uk-logo" href=""><img src="assets/image/logo-header.png" alt="logo-header"></a>
            </div>

            <div class="uk-navbar-right uk-visible@l">
                <ul class="uk-navbar-nav">
                    <li class="uk-active"><a href="">Home</a></li>
                    <li><a href="produtos">Produtos</a></li>
                    <li><a href="coloracao">Coloração</a></li>
                    <li><a href="seja-um-distribuidor">Seja um Distribuidor</a></li>
                    <li><a href="contato">Contato</a></li>
                    <li>
                        <a href="">BR<span uk-icon="chevron-down"></span></a>
                        <div uk-dropdown="mode: click">
                            <ul class="uk-nav uk-dropdown-nav">
                                <li><a href="en">EN</a></li>
                                <li><a href="es">ES</a></li>
                            </ul>
                        </div>
                    </li>
                    <li><a href="https://www.facebook.com/anabelecosmeticos" target="_blank" uk-icon="facebook"></a></li>
                    <li><a href="https://www.instagram.com/anabellecosmeticos/" target="_blank" uk-icon="instagram"></a></li>
                    <li><a href="https://www.youtube.com/channel/UCNFxXj5Q1Y2Ik1-vAChwf3A" target="_blank" uk-icon="youtube"></a></li>
                </ul>
            </div>

            <!-- Button OffCanvas -->
            <a class="uk-navbar-toggle uk-hidden@l uk-position-right" uk-navbar-toggle-icon uk-toggle="target: #offcanvas-nav-primary"><span class="off-canvas"></span></a>
        </nav>
    </div>
</header>


<!-- OFFCANVAS -->
<div id="offcanvas-nav-primary" uk-offcanvas="overlay: true">
    <div class="uk-offcanvas-bar uk-flex uk-flex-column">
        <a class="uk-offcanvas-brand" href=""><img src="assets/image/logo-footer.png" alt="logo-header"></a>
        <ul class="uk-nav uk-nav-primary uk-nav-left uk-margin-auto-vertical">
            <li class="uk-active"><a href="">Home</a></li>
            <li><a href="produtos">Produtos</a></li>
            <li><a href="coloracao">Coloração</a></li>
            <li><a href="seja-um-distribuidor">Seja um Distribuidor</a></li>
            <li><a href="contato">Contato</a></li>
            <li>
                <a href="">BR <span uk-icon="chevron-down"></span></a>
                <div uk-dropdown="mode:click">
                    <ul class="uk-nav uk-dropdown-nav">
                        <li><a href="en">EN</a></li>
                        <li><a href="es">ES</a></li>
                    </ul>
                </div>
            </li>

            <li class="uk-nav-divider"></li>
            <a href="https://www.facebook.com/anabelecosmeticos" target="_blank" uk-icon="icon: facebook"></a>
            <a href="https://www.instagram.com/anabellecosmeticos/" target="_blank" uk-icon="icon: instagram"></a>
            <a href="https://www.youtube.com/channel/UCNFxXj5Q1Y2Ik1-vAChwf3A" target="_blank" uk-icon="icon: youtube"></a>
        </ul>
    </div>
</div> 
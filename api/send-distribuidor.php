<?php

$nome = !empty($_POST['nome']) ? filter_var($_POST['nome'], FILTER_SANITIZE_STRING) : null;
$email = !empty($_POST['email']) ? filter_var($_POST['email'], FILTER_SANITIZE_STRING) : null;
$telefone = !empty($_POST['telefone']) ? filter_var($_POST['telefone'], FILTER_SANITIZE_STRING) : null;
$cidade = !empty($_POST['cidade']) ? filter_var($_POST['cidade'], FILTER_SANITIZE_STRING) : null;
$mensagem = !empty($_POST['mensagem']) ? filter_var($_POST['mensagem'], FILTER_SANITIZE_STRING) : null;

if (!$email) {
    //alerta caso o usuário não preencha todos os campos (inserir url)
    echo '<script>alert("Preencha todos os campos e tente novamente"); window.location.href="#";</script>';
} else {
    date_default_timezone_set('Brazil/East');
    
    //require das variaveis do servidor;
    require 'smtp.php';
    
    //inserir dados do servidor de e-mail;
    $smtp->host = '';
    $smtp->user = '';
    $smtp->pass = '';

    $date = date('d/m/Y');
    $hour = date('H:i');

    $msg  = "Olá.<br /><br />";
    $msg .= "\"{$nome}\" enviou uma mensagem pelo site e tem interesse em ser um distribuidor Anabelle.<br /><br />";
    $msg .= "E-mail: {$email}.<br /><br />";
    $msg .= "Telefone: {$telefone}.<br /><br />";
    $msg .= "Cidade: {$cidade}.<br /><br />";
    $msg .= "Mensagem: {$mensagem}. <br /><br />";
    $msg .= "Enviado em {$date} às {$hour}.";
    
    //trocar e-mail e nome da empresa;
    $success = $smtp->send('igor@layoutnet.com.br', "Layout - Mensagem enviada por \"{$nome}\"", $msg);

    if (!$success) {
        //em caso de sucesso no envio, irá aparecer um aviso e retornar a página indicada (inserir url);
        echo '<script>alert("Sua mensagem foi enviada com sucesso. Obrigado!"); window.location.href="#";</script>';
    } else {
        //em caso de erro, irá aparecer o aviso e retornar a página principal (inserir url);
        echo '<script>alert("Erro ao enviar a mensagem. Por favor, tente novamente mais tarde."); window.location.href="#";</script>';
    }
}
<!-- Produtos Profissionais Anabelle -->
<section id="produtos-profissionais">
    <div class="uk-section produtos-profissionais">
        <div class="uk-container">
            <h2 class="uk-text-center">produtos profissionais feito para profissionais</h2>
            <hr>
            <p class="uk-text-center">A linha de cosméticos da Anabelle foi totalmente pensada para atender um mercado específico, o de salões de beleza. A inovação e tecnologia da marca são empregadas para apresentar a melhor solução e garantia para o tratamento de beleza dos fios. </p>
        </div>
    </div>
    <div class="banner">
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/linha-de-produtos.png); height: 450px;">
            <div class="banner-text">
                <h2 class="uk-text-center">conheça nossa linha de produtos</h2>
            </div>
        </div>
    </div>
</section>

<!-- Carousel -->
<section id="carousel-clientes">
    <div class="uk-section carousel-clientes">
        <div class="uk-container">
            <div uk-slideshow="autoplay: true; autoplay-interval: 3000">
                <div class="uk-position-relative uk-visible-toggle uk-light" tabindex="-1">
                    <ul class="uk-slideshow-items">
                        <li>
                            <div class="uk-position-center uk-position-large uk-text-left uk-light text-carousel">
                                <p class="uk-text-center">“Vimos a Anabelle nascer e crescer! Temos a marca como o único fornecedor dentro do nosso salão. Nossa parceria começou há mais de 15 anos, onde sempre apoiamos e acreditamos no trabalho prestado pela marca. Só tenho a agradecer a qualidade dos produtos que trabalhamos, que veem fazendo sucesso entre as nossas clientes. Sempre apoiamos e acreditamos no trabalho da marca. Não abro a mão dos produtos e confio plenamente na Anabelle Cosméticos!”</p>
                                <h5 class="uk-text-center">Helena Modesto</h5>
                            </div>
                        </li>

                        <li>
                            <div class="uk-position-center uk-position-large uk-text-left uk-light text-carousel">
                                <p class="uk-text-center">“Vimos a Anabelle nascer e crescer! Temos a marca como o único fornecedor dentro do nosso salão. Nossa parceria começou há mais de 15 anos, onde sempre apoiamos e acreditamos no trabalho prestado pela marca. Só tenho a agradecer a qualidade dos produtos que trabalhamos, que veem fazendo sucesso entre as nossas clientes. Sempre apoiamos e acreditamos no trabalho da marca. Não abro a mão dos produtos e confio plenamente na Anabelle Cosméticos!”</p>
                                <h5 class="uk-text-center">Helena Modesto</h5>
                            </div>
                        </li>

                        <li>
                            <div class="uk-position-center uk-position-large uk-text-left uk-light text-carousel">
                                <p class="uk-text-center">“Vimos a Anabelle nascer e crescer! Temos a marca como o único fornecedor dentro do nosso salão. Nossa parceria começou há mais de 15 anos, onde sempre apoiamos e acreditamos no trabalho prestado pela marca. Só tenho a agradecer a qualidade dos produtos que trabalhamos, que veem fazendo sucesso entre as nossas clientes. Sempre apoiamos e acreditamos no trabalho da marca. Não abro a mão dos produtos e confio plenamente na Anabelle Cosméticos!”</p>
                                <h5 class="uk-text-center">Helena Modesto</h5>
                            </div>
                        </li>
                    </ul>
                </div>
                <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
            </div>
        </div>
    </div>
</section>

<!-- Footer -->
<footer>
    <section id="footer-default">
        <div class="banner-footer">
            <div class="uk-background-cover uk-height-medium uk-panel" style="background-image: url(../assets/image/banners/rodape.png);">
                <div class="uk-container">
                    <div class="uk-grid-small uk-child-width-expand@s uk-text-left" uk-grid>
                        <div class="uk-width-1-2@m">
                            <div class="uk-card">
                                <div class="address-block uk-padding">
                                    <img src="../assets/image/logo-footer.png" alt="logo-footer">
                                    <address>Rua Diamante, 41 Jd. Sontag</address>
                                    <address>Salto/SP - Cep 13326.110</address>
                                    <a href="mailto:atendimento@anabellecosmeticos.com.br">atendimento@anabellecosmeticos.com.br</a>
                                    <p>Matriz: 11 5654.8897 / 11 5543.8899</p>
                                    <a target="_blank" href="https://wa.me/5511966564131">WhatsApp: 11 9 6656.4131</a>
                                </div>
                            </div>
                        </div>

                        <div class="uk-width-auto@l uk-margin nav-footer">
                            <div class="uk-card">
                                <div class="navigation-footer uk-padding">
                                    <h5 class="uk-text-left@m">Navegue em Nosso Site</h5>
                                    <hr>
                                    <ul class="uk-text-left@m">
                                        <li><a href="">Quem Somos</a></li>
                                        <li><a href="">Produtos</a></li>
                                        <li><a href="">Coloração</a></li>
                                        <li><a href="">Seja um Revendedor</a></li>
                                        <li><a href="">Contato</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</footer>
<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/lavatorio-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/product-page/shampoo-hidratante.png" alt="shampoo-hidratante">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">shampoo hidratante</h2>
                        <hr>
                        <p>Desenvolvido com extrato de mandioca e silicones AMINOFUNCIONAL, o shampoo de mandioca treat hair, é rico em proteínas, vitaminas A, C e minerais, como ferro, cálcio e fósforo, elementos essenciais para a nutrição da fibra capilar. Promove uma limpeza suave e fortalecimento dos fios. Auxilia no crescimento dos cabelos.</p>

                        <h5>Volume</h5>
                        <p>5 Litros</p>

                        <h5>Precauções</h5>
                        <p>Manter fora do alcance das crianças. Uso externo. Em caso de contato com os olhos, lave com água em abundância. Em caso de irritação suspender o uso, e procure orientação médica.</p>

                        <h5>Modo de Usar</h5>
                        <p>Aplicar o Shampoo Hidratante com Extrato de Mandioca nos cabelos úmidos e massageando o necessário e enxague. Aplicar novamente e enxaguar se necessário.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua, Parfum, Citric Acid, Cocamidopropyl betaine, Dimethicone, Dissodium EDTA, Glycerin, PEG-150 Diesterate, Phenoxyethanol, Manihot Itulíssima Leaf Extract, Polyquaternium-7, Propylene Glycol, Sodium PCA, Ealeis Oliefera Kernel Oil, Guar Hydroxypropyl Trimonium Chloride, Sodium Laureth Sulfate, Glycol Diesterate, Cocamide DEA.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/perfect-waves-shampoo">
                        <img src="../assets/image/thumbs/perfect-waves-shampoo.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">perfect waves shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/moringa-oil">
                        <img src="../assets/image/thumbs/moringa-oil.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">moringa oil</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-mascara">
                        <img src="../assets/image/thumbs/mini-absolut-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-condicionador">
                        <img src="../assets/image/thumbs/perfect-waves-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/condicionador-hidratante">
                        <img src="../assets/image/thumbs/condicionador-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">condicionador hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-professional-condicionador">
                        <img src="../assets/image/thumbs/mini-absolut-pro-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair pro condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-leave-in">
                        <img src="../assets/image/thumbs/mini-absolut-leave-in.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat absolut leave-in</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/coloracao-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/product-page/titanium-colors.png" alt="titanium-colors">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">titanium colors caixa</h2>
                        <hr>
                        <p>A linha TITANIUM COLORS surpreende com a technology Angstrom ( tecnologia alemã, onde os ativos são 10 vezes menores que a nano tecnologia) e o BLEND 10 OIL, uma combinação de 10 óleos hidratantes, que proporcionam mais brilho e maior durabilidade da cor. </p>
                        <p>Com altíssima qualidade nos pigmentos e menor percentual de amônia, a coloração TITANIUM COLORS permite trabalhos mais sofisticados, possibilitando resultados de cor mais eficaz e duradouro.</p>

                        <h5>Volume</h5>
                        <p>Caixas de 60g</p>

                        <h5>Precauções</h5>
                        <p><strong>Cuidado:</strong> Contém substâncias passíveis de causar irritação na pele de determinadas pessoas. Antes de usar, faça a prova de toque. Pode causar reação alérgica. Não usar nos cílios e sobrancelhas. Em caso de contato com os olhos, lavar imediatamente com água em abundância. Não aplicar se o couro cabeludo estiver irritado ou lesionado. Enxaguar bem os cabelos após a aplicação. Manter fora do alcance das crianças. Usar luvas adequadas.</p>
                        <!-- Ancora do Modal -->
                        <a href="#modal-caution" uk-toggle><h6>Mais Informações</h6></a>
                        <!-- Modal -->
                        <div id="modal-caution" uk-modal>
                            <div class="uk-modal-dialog uk-modal-body">
                                <h2 class="uk-modal-title">Mais Informações - Precauções</h2>
                                <p>A coloração Titanium pode escurecer quantos tons desejar, ou os superclareadores clareá-los em até 4 tons em cabelos naturais, pois coloração não clareia coloração.</p>
                                <p>Não utilizar este produto imediatamente após um serviço químico de permanente, defrisagem ou alisamento e sobre cabelos em que tenha sido aplicar qualquer tipo de henê, tintura progressiva, ou à base de sais metálicos.</p>
                                <p>Manter o produto em local seco e fresco a temperatura ambiente. Proteger do calor e da luz solar direta.</p>
                                <p class="uk-text-right">
                                    <button class="uk-button uk-button-default uk-modal-close" type="button">Fechar</button>
                                </p>
                            </div>
                        </div>


                        <h5>Modo de Usar</h5>
                        <p>Em um recipiente não metálico, misturar a coloração Titanium Colors com a água oxigenada na proporção de 1 para 1.1/2, ou seja, para uma medida de coloração adicione uma medida e meia de água oxigenada, na volumagem adequada ao resultado desejado. Nos superclareadores a proporção é de 1 para 2, ou seja, para uma medida de coloração, adicionar duas medidas de água oxigenada 40 volumes.</p>

                        <!-- Ancora do Modal -->
                        <a href="#modal-infos" uk-toggle><h6>Mais Informações</h6></a>

                        <!-- Modal -->
                        <div id="modal-infos" uk-modal>
                            <div class="uk-modal-dialog uk-modal-body">
                                <h2 class="uk-modal-title">Mais Informações</h2>
                                <h5>Modo de Aplicar:</h5>
                                <p>Misture até obter um creme homogêneo. Aplique com pincel sobre os cabelos secos e não lavados e faça a aplicação imediatamente após a mistura. </p>
                                <h5>PROVA DE TOQUE:</h5>
                                <p>Prepare um pouco deste produto como se fosse utilizá-lo. Aplique no antebraço ou atrás da orelha. Deixe agir por 35 minutos. Lave o local. Aguarde 24 horas e se neste período surgir irritação na pele, coceira ou ardência no local ou na proximidade, fica comprovada a hipersensibilidade da pessoa ao produto e, portanto, o produto não deve ser utilizado. </p>
                                <h5>TESTE DE MECHA:</h5>
                                <p>Antes de usar recomendamos o teste de mecha, que deverá ser realizado a cada vez que for colorir o cabelo para determinar a resistência do fio e o resultado da cor. Permanentes, alisantes, coloração anterior e efeitos do sol podem afetar os resultados e o tempo de coloração. <br> Aplique o restante da mistura que foi previamente utilizada na prova de toque em uma pequena mecha na parte de trás do cabelo. Aguarde 35 minutos. Após o tempo de pausa lave e seque a mecha, faça a avaliação da cor obtida, o tempo de pausa e a reação dos cabelos. Caso ocorra aquecimento, quebra, dano do fio, ou o resultado indesejado da cor, o produto não deverá ser utilizado.</p>
                                <h5>Tempo de Pausa:</h5>
                                <p>30-40 minutos. <br> 40-45 minutos para superclareadores.</p>
                                <h5>Após o Tempo de Pausa</h5>
                                <p>Lave os cabelos abundantemente com água morna, até eliminar totalmente o produto. Em seguida lave com shampoo e depois aplique o condicionador e enxague. </p>
                                <p class="uk-text-right">
                                    <button class="uk-button uk-button-default uk-modal-close" type="button">Fechar</button>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua (Water), Cetearyl, Alcohol, Oleyl Alcohol, Ammonium Hydroxide, Lauramine Oxide, Cteareth-20, Behentrimonium Chloride, Amodimethicone, Glyceryl Stearate, Pentasodium Pentetate, Ammonium Lauryl Sulfate, Stearalkonium Choloride, Parfum, Buthypenyl, Methylpropional, D-Limonene, Linalool, Propylene Glycol, Acrylates/C10-30 Alkyl Acrylate Crosspolymer, Sodium Sulfite, Sodium Erythorbate, Mecadamia Ternigolia Seed Oil, Salvia Hispanica Seed Oil, Prunus Persica Kernel Oil, Corylus Avellana Seed Oil, Elaeis Fruit Oil, Brassica Napus Seed Oil, Argania Spinosa Kernel Oil.</p>
                <p><strong>PODE CONTER:</strong> P-Phenylenediamine, Resorcinol, 2,4-Diaminophenoxyethanol HCL, 2-Methylresorcionol, Toluene-2,5- Diamine Sulfate, 4-Amino-2-Hydroxytoluene, O-Aminophenol, P-Methylaminophenol Solfate, M-Aminophenol, P-Aminophenol, 2-Amino-3-Hydroxypyridine, N,N-Bis(2-Hydroxyethyl)-P-Phenylenediamine Sulfate, 2,6-Dihydroxyethylaminotoluene, 1-Hydroxyethyl 4,5-Diamino Pyrazole Sulfate, 2-Methyl-5-Hydroxyethylaminophenol, 5-Amino-6-Chloro-O-Cresol, Phenyl Methyl Pyrazolone, Basic Orange 31, Basic Red 51, Basic Yellow 87.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/treat-hair-plus-intensive-mask">
                        <img src="../assets/image/thumbs/treat-hair-plus-intensive-mask.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">treat hair plus intensive mask</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-ativador-de-cachos">
                        <img src="../assets/image/thumbs/perfect-waves-ativador-de-cachos.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves ativador de cachos</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/shampoo-hidratante">
                        <img src="../assets/image/thumbs/shampoo-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">shampoo hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-shampoo">
                        <img src="../assets/image/thumbs/mini-absolut-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-professional-mascara">
                        <img src="../assets/image/thumbs/mini-absolut-pro-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut pro máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-day-protector">
                        <img src="../assets/image/thumbs/mini-supra-day-protector.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra day protector</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-plus-capsulas">
                        <img src="../assets/image/thumbs/mini-treat-hair-plus-capsula.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair plus cápsulas</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
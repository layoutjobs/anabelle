<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/home-care-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/product-page/treat-hair-blond-condicionador.png" alt="treat-hair-blond-condicionador">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">treat hair blond condicionador</h2>
                        <hr>
                        <p>Um produto com fórmula exclusiva que reduz progressivamente os tons amarelados indesejados dos cabelos loiros ou com mechas. Enriquecido com pigmentos violetas selecionados e creatina que ajuda na recuperação dos fios loiros e descoloridos.</p>

                        <h5>Volume</h5>
                        <p>300ml</p>

                        <h5>Precauções</h5>
                        <p>Mantenha em local fresco ao abrigo de luz, mantenha fora do alcance de crianças, em caso de contato com os olhos, lave abundantemente com água.</p>

                        <h5>Modo de Usar</h5>
                        <p>Após o uso do Shampoo Blond Treat Hair, com os cabelos úmidos, aplique uma pequena quantidade do Condicionador Blond Treat Hair massageando de 2 a 5 minutos, enxague em seguida.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua, Parfum, Acid Violet 43, Cyclomethicone, Dimethicone, Phenyltrimeticone, Trimethylsilylamodimethicone, Hydrolizated Keratin, Amodimethycone, Cetrimonium Chloride, Dissodium Edta, Cetyl, Alcohol, Methylparaben, Propylparaben, Propyleneglycol, Paraffinliquid, Isopropylpalmitate, Citric Acid, Creatine.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/mascara-hidratante">
                        <img src="../assets/image/thumbs/mascara-hidratante.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">máscara hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-day-protector">
                        <img src="../assets/image/thumbs/mini-supra-day-protector.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra day protector</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/moringa-oil">
                        <img src="../assets/image/thumbs/moringa-oil.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">moringa oil</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-leave-in">
                        <img src="../assets/image/thumbs/mini-absolut-leave-in.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut leave-in</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-light-po-descolorante">
                        <img src="../assets/image/thumbs/mini-po-descolorante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">pó descolorante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-plus-intensive-mask">
                        <img src="../assets/image/thumbs/treat-hair-plus-intensive-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair plus intensive mask</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-blond-shampoo">
                        <img src="../assets/image/thumbs/treat-hair-blond-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair blond shampoo</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/tratamento-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/product-page/absolut-pro-shampoo.png" alt="absolut-pro-shampoo">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">treat hair absolut pro shampoo</h2>
                        <hr>
                        <p>O redutor de volume Treat Liss é um produto rico em Óleo de Ojon Creatina e nano particulas de tratamento que juntos promovem um alinhamento das cutículas, corrigindo a porosidade dos cabelos, tornando-os, mais macios sedosos e com muito brilho. Por conter em sua composição Óleo de Ojon e Pró-liss o redutor de volume Treat Liss é um produto de alto rendimento e com grande poder de alinhamento capilar, sua fórmula é de fácil aplicação.</p>

                        <h5>Volume</h5>
                        <p>1 Litro</p>

                        <h5>Precauções</h5>
                        <p>Evitar o couro cabeludo. Manter fora do alcance de crianças. Em caso de contato com os olhos, enxaguar abundantemente, havendo algum tipo de irritação procurar orientação médica.</p>

                        <h5>Modo de Usar</h5>
                        <p>Após a lavagem dos cabelos com o shampoo, aplicar o redutor de volume e deixar por agir 15 minutos, tirar o excesso com uma toalha e escovar. Pranchar 15 vezes cada mecha fina. Usar um creme após o processo e enxaguar.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua, Propylene Glycol, PEG-14M, Hidroxyethylcellulose, Cetrimonium Chloride, Guar Hydroxypropyltrimonium Chloride, DEA PG-Propyl PEG/PPG-12/21 Dimethicone, Parfum, PEG-40 Castor Oil, Phenoxyethanol, Dissodium EDTA, Methylisothiazolinone (and) Methylchloroisothiazolinone, Macadamia Ternifolia Seed Oil, Hydrolized Wheat Protein, Hydrolized Keratin, Persea Gratissima Oil, Citric Acid, Ethylhexyl Methoxycinnamate, Linalool, Hexyl Cinnamal, Butylphenyl Methylpropional, D-Limonene, Citronellol, Coumarin, Geraniol, CI19140, CI16255.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/condicionador-hidratante">
                        <img src="../assets/image/thumbs/condicionador-hidratante.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">condicionador hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-blond-mascara">
                        <img src="../assets/image/thumbs/treat-hair-blond-mascara.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair blond máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-ativador-de-cachos">
                        <img src="../assets/image/thumbs/perfect-waves-ativador-de-cachos.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves ativador de cachos</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-leave-in">
                        <img src="../assets/image/thumbs/mini-absolut-leave-in.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut leave-in</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/ox-titanium-colors">
                        <img src="../assets/image/thumbs/mini-ox-titanium.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">ox titanium colors</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-professional-mascara">
                        <img src="../assets/image/thumbs/mini-absolut-pro-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut pro mask</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/black-platinum-progressive-brush">
                        <img src="../assets/image/thumbs/black-platinum.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">black platinum</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
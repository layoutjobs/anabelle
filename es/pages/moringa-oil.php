<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/home-care-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/product-page/moringa-oil.png" alt="moringa-oil">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">moringa oil</h2>
                        <hr>
                        <p>O Moringa Oil é óleo extraído da semente da planta Moringa Oleífera, também conhecida como a árvore da vida. Patenteado, o óleo de moringa da treat hair possui alta estabilidade oxidativa, é rico em ácidos graxos como ômega 9 e álcool berrênico e, portanto, acaba por conferir maciez e emoliência para formulações destinadas aos cuidados da pele e dos cabelos.
                            Derivado de óleos vegetai, o álcool berrênico, nesta composição tem a função de proporcionar uma rica emoliência aos fios sem deixar aquela sensação de peso. Quando utilizado no cabelo, o Moringa Oil age como um reparador de pontas, tornando mais fácil a penteabilidade a seco e aumentando o brilho dos cabelos. Já para os cuidados com a pele, este óleo acaba por promover uma intensa nutrição, emoliência, maciez e sedosidade.
                        </p>

                        <h5>Volume</h5>
                        <p>30/10 ml</p>

                        <h5>Precauções</h5>
                        <p>Manter protegido da luz e fora do alcance das crianças, uso externo.</p>

                        <h5>Modo de Usar</h5>
                        <p>Dilua algumas gostas na palma da mão e aplique no cabelo seco ou molhado, dosando de acordo com o comprimento e textura do fio.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Isopropyl Palmitate, Propylparaben, BHT, Cyclopentasiloxane / Dimethiconol, Dimethicone, Cyclopentasiloxane / Dimethiconol, Dimethicone, Cyclopentasiolxane, Parfum (Hydroxycitronellal, Linalool, Coumarin, Citronellol, Benzyl Alcohol, Alpha Isomethyl Ionone, Geraniol, Cynnamyl Alcohol, Butylphenyl Methylproponional, Amyl Cinnamal, Eugenol, Hydroxyisohexyl 3Cyclohexene Carboxaldehyde, Moringa Oleifera Seed Oil. </p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/treat-hair-plus-capsulas">
                        <img src="../assets/image/thumbs/mini-treat-hair-plus-capsula.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">treat hair plus cápsulas</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-professional-mascara">
                        <img src="../assets/image/thumbs/mini-absolut-pro-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut pro máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-pro-shampoo">
                        <img src="../assets/image/thumbs/mini-absolut-pro-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut pro shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/shampoo-hidratante">
                        <img src="../assets/image/thumbs/shampoo-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">shampoo hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-sinergica-blond">
                        <img src="../assets/image/thumbs/mini-supra-blond.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra sinérgica blond</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="">
                        <img src="../assets/image/thumbs/treat-hair-blond-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair blond shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="">
                        <img src="../assets/image/thumbs/treat-hair-blond-mascara.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair blond máscara</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
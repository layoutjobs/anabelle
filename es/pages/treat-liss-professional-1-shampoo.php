<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/reducao-de-volume-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/product-page/treat-liss-pro-shampoo.png" alt="treat-liss-pro-shampoo">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">treat liss pro shampoo</h2>
                        <hr>
                        <p>O Shampoo Anti-Resíduos limpa profundamente o cabelo, retirando o excesso de produtos químicos que impregnam os fios.</p>

                        <h5>Volume</h5>
                        <p>1 Litro</p>

                        <h5>Precauções</h5>
                        <p>Evitar o couro cabeludo. Manter fora do alcance de crianças. Em caso de contato com os olhos, enxaguar abundantemente, havendo algum tipo de irritação procurar orientação médica.</p>
                        <p>Em caso de contato com os olhos, enxaguar abundantemente. Em caso de irritação, suspender o uso. Ao persistirem os sintomas, procure orientação médica. Não ingerir. Conserva em lugar seco, arejado e ao abrigo da luz. Uso externo. MANTER FORA DO ALCANCE DE CRIANÇAS.</p>

                        <h5>Modo de Usar</h5>
                        <p>Aplicar o Shampoo no cabelo molhado, massagear e enxaguar.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Water/aqua, sodium laureth sulfate, cocamidec, Dea, Citricacid, Sodium chloride, dissodium EDTA, Methilparaben, Fragrance/parfum, Sodium Palystrene sulfonate.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/treat-hair-absolut-professional-condicionador">
                        <img src="../assets/image/thumbs/mini-absolut-pro-condicionador.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">treat hair absolut pro condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-blond-shampoo">
                        <img src="../assets/image/thumbs/treat-hair-blond-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair blond shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/condicionador-hidratante">
                        <img src="../assets/image/thumbs/condicionador-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">condicionador hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-blond-mascara">
                        <img src="../assets/image/thumbs/treat-hair-blond-mascara.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair blond máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-shampoo">
                        <img src="../assets/image/thumbs/perfect-waves-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-sinergico-expert">
                        <img src="../assets/image/thumbs/mini-supra.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra sinérgico expert</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/titanium-colors-caixa">
                        <img src="../assets/image/thumbs/mini-titanium-colors.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">titanium colors</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
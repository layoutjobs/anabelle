<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/home-care-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/product-page/absolut-leave-in.png" alt="absolut-leave-in">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">treat hair absolut leave-in</h2>
                        <hr>
                        <p>Forma um efeito de filme protetor nos fios, auxiliando na penteabilidade e maleabilidade, tornando os cabelos disciplinados e brilhantes, podendo ser usado antes da prancha e escova.</p>

                        <h5>Tamanhos</h5>
                        <p>250ml | 8,45 fl Oz</p>

                        <h5>Precauções</h5>
                        <p>Mantenha em local fresco ao abrigo de luz, mantenha fora do alcance de crianças, em caso de contato com os olhos, lave abundantemente com água.</p>

                        <h5>Modo de Usar</h5>
                        <p>Aplique uma pequena quantidade em toda a extensão dos fios, finalize como desejar.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua – cetearyl alcohol – petrolatum – glycerin – polyquaternium – 7 – cetearyl alcohol / behentrimonium methosulfate – cetrimonium chloride – c13-14 alkane – cyclomethicone/dimethicone – parfum: butylphenyl methylpropional / citral/hexyl cinnamal/limonene/linalool/ methyl 2-octynoate. Ci19140- stearamidopropyl -  dimethylamine- lactic acid – panthenol – ci15985 – phenyl trimethicone- disodium edta – methylchloroisothiazolinone / methyllisothiazolinone/ magnesium chloride/ magnesiumnitrate – guar hydroxypropyltrimonium chloride – bht.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/titanium-colors-caixa">
                        <img src="../assets/image/thumbs/mini-titanium-colors.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">titanium colors</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-shampoo">
                        <img src="../assets/image/thumbs/perfect-waves-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-blond-mascara">
                        <img src="../assets/image/thumbs/treat-hair-blond-mascara.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair blond máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-professional-condicionador">
                        <img src="../assets/image/thumbs/mini-absolut-pro-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">absolut pro condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-ativador-de-cachos">
                        <img src="../assets/image/thumbs/perfect-waves-ativador-de-cachos.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves ativador de cachos</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/ox-titanium-colors">
                        <img src="../assets/image/thumbs/mini-ox-titanium.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">ox titanium colors</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-professional-mascara">
                        <img src="../assets/image/thumbs/mini-absolut-pro-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut pro máscara</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
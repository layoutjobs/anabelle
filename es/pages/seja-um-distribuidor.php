<!-- BannerTopo -->
<section id="banner-topo-distribuicao">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/distribuidor.png); height: 700px;">
            <div class="intro-text">
                <h1>Leve a autoestima por onde passar</h1>
                <p>Seja um distribuidor Anabelle</p>
                <a class="uk-button uk-button-default" href="#contato-distribuidor" uk-scroll>QUERO SER UM DISTRIBUIDOR</a>
            </div>
        </div>
    </div>
</section>

<!-- Main -->
<section id="seja-distribuidor">
    <div class="uk-section distribuidor">
        <div class="uk-container">
            <h2 class="uk-text-center">Uma linha completa para os melhores salões de beleza</h2>
            <hr>
            <p class="uk-text-center uk-padding">A história de expansão da Anabelle pode ter você como personagem! A Anabelle Cosméticos conta com um time de distribuidores da beleza, que levam uma linha completa de produtos para os melhores salões de beleza do Brasil. Aumente sua renda ao apresentar um produto inovador, uma linha completa de alta qualidade que vem se destacando no mercado da beleza. </p>

            <!-- Circles -->
            <div class="uk-child-width-expand@s uk-text-center" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/icon-empreender.png" alt="empreender">
                        <h4>EMPREENDER</h4>
                        <p class="uk-padding-small">A Anabelle deseja que seu distribuidor vá além das vendas. Seja um empreendedor com estratégias, treinamentos e liderança junto a uma marca que segue em crescimento.</p>
                    </div>
                </div>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/icon-lucratividade.png" alt="lucratividade">
                        <h4>LUCRATIVIDADE</h4>
                        <p class="uk-padding-small">O Brasil ocupa a 3ª posição no ranking de consumo de cosméticos. A alta lucratividade do ramo e uma marca pioneira no mercado, correspondem a uma margem bruta de 100 a 150% conforme o produto.</p>
                    </div>
                </div>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/icon-retorno.png" alt="retorno-rapido">
                        <h4>RETORNO RÁPIDO</h4>
                        <p class="uk-padding-small">Com um alcance e visibilidade do produto, a Anabelle possui um sistema de vendas diretas a salões de beleza com lucratividade e rentabilidade, garantindo um retorno rápido do investimento. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Banner -->
<section id="banner-quem-clientes">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/distribuidor-clientes.png); height: 450px;">
            <div class="intro-text">
                <h2 class="uk-text-center uk-padding">quem serão seus clientes?</h2>
                <p class="uk-text-center">Salões de Beleza, perfumarias com espaço destinado ao profissional e micro distribuidores atuantes em sua região.</p>
            </div>
        </div>
    </div>
</section>

<!-- intro-counter -->
<section id="intro-counter">
    <div class="uk-section">
        <div class="uk-container uk-padding-large">
            <h2 class="uk-text-center">O que há de especial em nossos produtos?</h2>
            <hr>
            <p class="uk-text-center">Devido a alta qualidade e tecnologia dos mais de 50 produtos inovadores e sua ótima relação custo x benefício, fazem os produtos Anabelle Cosméticos, serem presença garantida nos melhores salões de cabeleireiros.</p>
        </div>
    </div>
</section>

<!-- Counter Estados -->
<section id="banner-counter">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/distribuidor-numeros.png); height: 100%; background-size: cover;">
            <div class="uk-container">
                <div class="uk-child-width-expand@l uk-text-center" uk-grid>
                    <div>
                        <div class="uk-card">
                            <div class="block-color">
                                <p class="intro-counter">estamos em</p>
                                <p class="counter">13</p>
                                <p class="intro-state">Estados do Brasil</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <div class="block-color">
                                <p class="intro-counter">mais de</p>
                                <p class="counter">200</p>
                                <p class="intro-state">Revendedores</p>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <div class="block-color">
                                <p class="intro-counter">mais de</p>
                                <p class="counter">1700</p>
                                <p class="intro-state">Salões Parceiros</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Download Catalogo -->
<section id="download-catalogo">
    <div class="uk-section catalogo">
        <div class="uk-container">
            <h2 class="uk-text-center">Baixe nosso Catalogo</h2>
            <hr>
            <p class="uk-text-center">Clique na imagem abaixo, e conheça ainda mais sobre nossos produtos!</p>
        </div>
    </div>
</section>

<!-- Form Contato - Distribuidor -->
<section id="contato-distribuidor">
    <div class="uk-section">
        <div class="uk-container">
            <h2 class="uk-text-center">Seja um Distribuidor</h2>
            <hr>
            <p class="uk-text-center">Tenha um negócio de sucesso na sua cidade! <br> Preencha o formulário abaixo que entraremos em contato. </p>
            <br>
            <!-- Formulário de Contato -->
            <div class="block-form">
                <form class="uk-grid-small formphp" name="form" method="POST" action="../api/send-distribuidor.php" uk-grid>
                    <div class="uk-width-1-2@l">
                        <input class="uk-input" type="text" name="nome" placeholder="NOMBRE">
                        <input class="uk-input" type="email" name="email" placeholder="E-MAIL">
                        <input class="uk-input" type="text" name="telefone" placeholder="TELÉFONO">
                        <input class="uk-input" type="text" name="cidade" placeholder="CIUDAD/ESTADO">
                    </div>
                    <div class="uk-width-1-2@l uk-text-center">
                        <textarea class="uk-textarea" rows="7" name="mensagem" placeholder="MENSAJE"></textarea>
                        <button class="uk-button uk-button-default btn-enviar" type="submit">Enviar</button>
                    </div>
                    <div class="uk-width-1-1@l">
                        <label class="dont-display">Se você não é um robô, deixe em branco.</label>
                        <input type="text" class="dont-display" name="leaveblank">
                        <label class="dont-display">Se você não é um robô, não mude este campo.</label>
                        <input type="text" class="dont-display" name="dontchange" value="http://">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/home-care-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card description-product">
                        <img src="../assets/image/product-page/perfect-waves-condicionador.png" alt="perfect-waves-condicionador">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">perfect waves condicionador</h2>
                        <hr>
                        <p>Um produto com fórmula exclusiva, enriquecido com Pantenol (vitamina B5) Óleo de abacate (Avocado), Extrato de Linum e Salvia Hispânica. Condiciona, hidrata e deixa macio, os cabelos ondulados, encaracolados ou cacheados.</p>

                        <h5>Volume</h5>
                        <p>250ml</p>

                        <h5>Precauções</h5>
                        <p>Mantenha em local fresco ao abrigo de luz, mantenha fora do alcance de crianças, em caso de contato com os olhos, lave abundantemente com água.</p>

                        <h5>Modo de Usar</h5>
                        <p>Após o uso do Shampoo Perfect Waves, com os cabelos úmidos, aplique uma pequena quantidade do Condicionador Perfect Waves, massageando de 2 a 5 minutos, enxague em seguida.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua, Cetearyl alcohol, chloride, centrimonium chloride, propylene glycol, stearamidopropyl dimethylamine, lactic acid, persea gratíssima oil, parfum: benzyl alcohol/benzyl salicylate/citral/citronellol/geraniol/hexyl cinnamal/hydroxycitronellal/hidroxyisohe xyl3, cyclohexene carboxaldehyde/limonene/linalool,phenyl trimethicone, linum usitatissimum (linseed) seed extract/salvia hispanica seed extract,panthenol, methylchoroisothiazolinone/methylisothiazolinone/magnesium chloride/magnesium nitrate, disodium edta.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/condicionador-hidratante">
                        <img src="../assets/image/thumbs/condicionador-hidratante.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">condicionador hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-light-po-descolorante">
                        <img src="../assets/image/thumbs/mini-po-descolorante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">pó descolorante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-condicionador">
                        <img src="../assets/image/thumbs/mini-absolut-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-liss-pro-shampoo">
                        <img src="../assets/image/thumbs/treat-liss-pro-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat liss pro shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-day-protector">
                        <img src="../assets/image/thumbs/mini-supra-day-protector.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra day protector</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-shampoo">
                        <img src="../assets/image/thumbs/mini-absolut-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-professional-condicionador">
                        <img src="../assets/image/thumbs/mini-absolut-pro-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut pro condicionador</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
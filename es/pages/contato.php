<section id="banner-topo-contato">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/contatos.png); height: 250px;">
            <div class="intro-text">
                <h1 class="uk-text-center">Contacto</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<!-- Main -->
<section id="contato">
    <div class="uk-section contato">
        <div class="uk-container uk-container-medium">
            <div class="block-form">
                <div class="uk-child-width-expand@s uk-text-center" uk-grid>
                    <div>
                        <div class="uk-card">
                            <form class="uk-grid-small formphp" name="form" method="POST" action="../api/send-contact.php" uk-grid>
                                <div class="uk-width-1-2@s">
                                    <input class="uk-input" type="text" name="name" placeholder="NOMBRE" required>
                                </div>
                                <div class="uk-width-1-2@s">
                                    <input class="uk-input" type="text" name="telefone" placeholder="TELÉFONO" required>
                                </div>
                                <div class="uk-width-1-2@s">
                                    <input class="uk-input" type="email" name="email" placeholder="E-MAIL" required>
                                </div>
                                <div class="uk-width-1-2@s">
                                    <input class="uk-input" type="text" name="assunto" placeholder="SUJETO" required>
                                </div>
                                <div class="uk-width-1-2@s">
                                    <label class="dont-display">Se você não é um robô, deixe em branco.</label>
                                    <input type="text" class="dont-display" name="leaveblank">
                                </div>
                                <div class="uk-width-1-2@s">
                                    <label class="dont-display">Se você não é um robô, não mude este campo.</label>
                                    <input type="text" class="dont-display" name="dontchange" value="http://">
                                </div>
                                <div class="uk-margin uk-width-1-1@s">
                                    <textarea class="uk-textarea" rows="6" name="mensagem" placeholder="MENSAJE" required></textarea>
                                </div>
                                <div class="btn-enviar">
                                    <button class="uk-button uk-button-default" type="submit" id="btn">Enviar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <h4>Anabelle Cosméticos</h4>
                            <br>
                            <div class="circle">
                                <img src="../assets/image/icon-email.png" alt="icon-email">
                            </div>
                            <div class="block-email">
                                <p class="uk-text-center@s uk-text-left@m">E-mail</p>
                                <a href="mailto:contato@anabelecosmeticos.com.br">contato@anabelecosmeticos.com.br</a>
                            </div>

                            <div class="circle">
                                <img class="icon-tel" src="../assets/image/icon-tel.png" alt="icon-tel">
                            </div>
                            <div class="block-tel">
                                <p class="uk-text-center@s uk-text-left@m">Teléfono</p>
                                <a href="tel:551140212294">+55 11 4021-2294</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Perguntas Frequentes -->
<section id="perguntas-frequentes">
    <div class="uk-section perguntas-frequentes">
        <div class="uk-container">
            <h1 class="uk-text-center">Perguntas Frequentes</h1>
            <hr>
            <br> <br>
            <div class="block-accordion">
                <div class="uk-grid-small uk-child-width-expand@s uk-text-left" uk-grid>
                    <div>
                        <div class="uk-card">
                            <ul uk-accordion>
                                <li>
                                    <a class="uk-accordion-title" href="#">Os produtos anabelle são produzidos no Brasil?</a>
                                    <div class="uk-accordion-content">
                                        <p>Todos os produtos Anabelle Cosméticos, são produzidos nacionalmente com mais alto nível de qualidade e segurança.</p>
                                    </div>
                                </li>

                                <li>
                                    <a class="uk-accordion-title" href="#">Quais produção ANABELLE são veganas?</a>
                                    <div class="uk-accordion-content">
                                        <p>Todos os produtos Anabelle Cosméticos, são produzidos nacionalmente com mais alto nível de qualidade e segurança.</p>
                                    </div>
                                </li>

                                <li>
                                    <a class="uk-accordion-title" href="#">POSSO VENDER PRODUTOS ANABELLE?</a>
                                    <div class="uk-accordion-content">
                                        <p>Todos os produtos Anabelle Cosméticos, são produzidos nacionalmente com mais alto nível de qualidade e segurança.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <ul uk-accordion>
                                <li>
                                    <a class="uk-accordion-title" href="#">Qual é a ação da nutrição dos meus fios?</a>
                                    <div class="uk-accordion-content">
                                        <p>Todos os produtos Anabelle Cosméticos, são produzidos nacionalmente com mais alto nível de qualidade e segurança.</p>
                                    </div>
                                </li>

                                <li>
                                    <a class="uk-accordion-title" href="#">ONDE POSSO ENCONTRAR PRODUTOS ANABELLE?</a>
                                    <div class="uk-accordion-content">
                                        <p>A nutrição capilar tem como principal finalidade repor os lipídeos aos fios. Lipídeos são gorduras produzidas pelas glândulas sebáceas do couro cabeludo e tem como função conferir uma camada protetora aos cabelos, deixando-os sempre macios, brilhantes e com a cutícula alinhada.</p>
                                    </div>
                                </li>

                                <li>
                                    <a class="uk-accordion-title" href="#">POSSO VENDER PRODUTOS ANABELLE?</a>
                                    <div class="uk-accordion-content">
                                        <p>Todos os produtos Anabelle Cosméticos, são produzidos nacionalmente com mais alto nível de qualidade e segurança.</p>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
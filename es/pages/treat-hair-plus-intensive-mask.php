<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/tratamento-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/product-page/treat-hair-plus-intensive-mask.png" alt="treat-hair-plus-intensive-mask">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">treat hair plus intensive mask</h2>
                        <hr>
                        <p>Desenvolvido com ativos nobres hidratantes e aminoácidos micro encapsulados reconstrutores, que trabalhando juntos, garantem reparação intensa, proporcionando cabelos macios, brilhantes e saudáveis com controle do frizz, sem deixa-los pesados.</p>

                        <h5>Volume</h5>
                        <p>1kg</p>
                        
                        <h5>Precauções</h5>
                        <p>Mantenha armazenado em local seco protegido da luz e calor, fora do alcance de crianças. Produto de uso externo. Uso exclusivo profissional. Não aplique o produto se o couro cabeludo estiver irritado ou lesionado.</p>

                        <h5>Modo de Usar</h5>
                        <p>Com o cabelo limpo e úmido, aplicar uma pequena quantidade na palma da mão, friccionar uma mão na outra para liberação dos aminoácidos micro encapsulados. Em seguida, aplicar em toda extensão dos fios, massagear e deixar agir de 10 a 15 minutos, enxaguar e finalizar com ou sem fonte de calor.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua, Parfum, Cyclomethicone, Dimethicone, propyleneglycol, Phenyltrimeticone, Trimethylsilylamodimethicone, hydrolizated keratin, amodimethycone, glycerin, cetrimonium chloride, dissodium edta, cetyl alcohol, elaeis oleífera kerenl oil, Methylparaben, propylparaben, paraffin Liquid, Stearamidopropyl, Dimethylamine, isopropylpalmitate, citricacid.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/titanium-colors-caixa">
                        <img src="../assets/image/thumbs/mini-titanium-colors.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">titanium colors</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-sinergica-blond">
                        <img src="../assets/image/thumbs/mini-supra-blond.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra sinérgico blond</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-mascara">
                        <img src="../assets/image/thumbs/mini-absolut-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-shampoo">
                        <img src="../assets/image/thumbs/mini-absolut-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-pro-shampoo">
                        <img src="../assets/image/thumbs/mini-absolut-pro-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat Hair Absolut Pro Shampoo </h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/shampoo-hidratante">
                        <img src="../assets/image/thumbs/shampoo-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">shampoo hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-blond-mascara">
                        <img src="../assets/image/thumbs/treat-hair-blond-mascara.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair blond máscara</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/coloracao-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/product-page/treat-hair-light-descolorante.png" alt="treat-hair-light-descolorante">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">treat hair light descolorante</h2>
                        <hr>
                        <p>Conserve o produto em local seco, arejado e ao abrigo da luz e do calor. Após o uso, lacre-o bem, pois o contato com o meio ambiente pode reduzir a sua eficácia.</p>

                        <h5>Volume</h5>
                        <p>500g</p>

                        <h5>Precauções</h5>
                        <p><strong>CUIDADO!</strong> Contém substâncias passíveis de causar irritação na pele de determinadas pessoas. Não aplique em sobrancelhas ou cílios. Este produto não deve ser utilizado antes ou após qualquer tipo de tratamento químico, tais como tintura progressiva a base de sais metálicos, permanente ou alisamento. O uso indevido deste produto pode causar danos ao organismo. Não reaproveite a embalagem. Não se recomenda o uso ou a aplicação desse produto em gestantes e lactantes. Não usar em crianças. Não aplique o produto se o couro cabeludo estiver irritado ou lesionado. Em caso de contato com os olhos ou mucosas, lave imediatamente com água em abundância até a remoção completa do produto e, se necessário, procure um médico. Mantenha o produto fora do alcance das crianças. Contém amônia. </p>

                        <h5>Modo de Usar</h5>
                        <p>Após a lavagem dos cabelos com o shampoo, aplicar o redutor de volume e deixar por agir 15 minutos, tirar o excesso com uma toalha e escovar. Pranchar 15 vezes cada mecha fina. Usar um creme após o processo e enxaguar.</p>
                        <!-- Ancora do Modal -->
                        <a href="#modal-info" uk-toggle>
                            <h6>Mais Informações</h6>
                        </a>

                        <!-- Modal -->
                        <div id="modal-info" uk-modal>
                            <div class="uk-modal-dialog uk-modal-body">
                                <h2 class="uk-modal-title">Mais Informações - Modo de Usar</h2>
                                <p><strong>PARA DESCOLORAÇÃO TOTAL:</strong> Recomenda-se o uso de uma medida (50g) do Pó Descolorante Treat Hair Light, acrescentando 150ml de Água Oxigenada de 10, 20, 30 ou 40 volumes. Iniciando a aplicação na nuca ou nas partes mais escuras.</p>
                                <p><strong>TEMPO DE PAUSA:</strong> Controlar em função do grau de clareamento desejado, mas não ultrapassar o tempo máximo de 45 minutos. Após o tempo de ação, enxaguar abundantemente.</p>
                                <p><strong>PROVA DE TOQUE:</strong> Preparar uma pequena quantidade de produto e aplicar no antebraço ou atrás da orelha. Aguardar 45 minutos e enxaguar. Se no período de 24 horas não apresentar irritação, o produto poderá ser aplicado.</p>
                                <p><strong>TESTE DE MECHA:</strong> Misture um pouco do produto e aplique em uma mecha fina de cabelo no alto da cabeça, deixe agir pelo tempo de ação faça a PROVA DO TOQUE e o TESTE DE MECHA. Este produto deve ser utilizado para o fim a que se destina, sendo PERIGOSO para qualquer outro emprego. USO EXTERNO.</p>
                                <p class="uk-text-right">
                                    <button class="uk-button uk-button-default uk-modal-close" type="button">Fechar</button>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Sodium Silicate, Potassium Persulfate, Ammonium Persulfate, Sodium Persulfate, Magnesium Trisilicate, Magnesium Silicate, Paraffinum Liquidum, Sodium Stearate, Cyamopsis Tetragonolobus Gum, Corn Starch, Magnesium Carbonate, Silica, Sodium Laureth Sulfate, Tetrasodium Edta, Xanthan Gum. Ci77007, Parfum, Keratin. </p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/treat-hair-blond-mascara">
                        <img src="../assets/image/thumbs/treat-hair-blond-mascara.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">treat hair blond máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/condicionador-hidratante">
                        <img src="../assets/image/thumbs/condicionador-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">condicionador hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-sinergica-expert">
                        <img src="../assets/image/thumbs/mini-supra.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra sinérgica expert</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-condicionador">
                        <img src="../assets/image/thumbs/mini-absolut-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-mascara">
                        <img src="../assets/image/thumbs/mini-absolut-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-condicionador">
                        <img src="../assets/image/thumbs/perfect-waves-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/mascara-hidratante">
                        <img src="../assets/image/thumbs/mascara-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">máscara hidratante</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
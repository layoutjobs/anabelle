<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/reducao-de-volume-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/product-page/black-platinum.png" alt="black-platinum-intern">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">black platinum progressive brush</h2>
                        <hr>
                        <p>O redutor de volume Black Platinum, foi desenvolvida com pró-liss e óleo de Ojon, dois poderosos ativos que juntos promovem um liso perfeito e tratado, por conter em sua composição uma grande concentração de pigmentos Black, o redutor de volume matizador Black Platinum, é o produto ideal para reduzir, tratar e matizar os fios com tons amarelados e indesejados. O efeito matizador é duradouro e tem um resultado imediato. O redutor Black Platinum é recomendado para quem busca excelência em cosméticos, além de promover redução de volume, ele hidrata, dá brilho, sela as cutículas impedindo a desidratação dos fios, prolongando ainda mais o efeito de liso, matiza os tons amarelados, tornando-se possível ter um cabelo loiro, liso, saudável e platinado.</p>

                        <h5>Tamanhos</h5>
                        <p>1L/33,81 Floz</p>
                        
                        <h5>Precauções</h5>
                        <p>Evitar o couro cabeludo, manter fora do alcance de crianças, em caso de contato com os olhos, enxaguar abundantemente, havendo algum tipo de irritação procurar orientação médica.</p>

                        <h5>Modo de Usar</h5>
                        <p>Após a lavagem dos cabelos com o shampoo Treat Liss, aplicar o Redutor de volume Black Platinum e deixar agir por 15 á 20 minutos, tirar o excesso com uma toalha, aplicar o finish spray Day Protector e escovar em seguida, pranchar 15 vezes cada mecha fina até obter o liso e brilho desejado.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Water/aqua, propyleneglicol, cetrimoniun cloryde, trietanolamina, dissodium EDTA, BHT, Polyquaternium 10, Cetearyl alcohol, Ceterareth 20, Mineral Oil, Sweetalmond oil, Dimethicone and ciclomethycone, keratin, Glioxilic acid, Polyquaternium 7, Poliquaternuim 33, Fragrance perfum. </p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/treat-hair-plus-intensive-mask">
                        <img src="../assets/image/thumbs/treat-hair-plus-intensive-mask.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">treat hair intensive mask</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-mascara">
                        <img src="../assets/image/thumbs/mini-absolut-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair pro absolut máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-shampoo">
                        <img src="../assets/image/thumbs/perfect-waves-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-plus-capsulas">
                        <img src="../assets/image/thumbs/mini-treat-hair-plus-capsula.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair plus cápsulas</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-pro-shampoo">
                        <img src="../assets/image/thumbs/mini-absolut-pro-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat absolut shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/ox-titanium-colors">
                        <img src="../assets/image/thumbs/mini-ox-titanium.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">ox titanium colors</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-leave-in">
                        <img src="../assets/image/thumbs/mini-absolut-leave-in.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut leave-in</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
<!-- Banner Topo Produtos -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/produtos.png); height: 250px;">
            <div class="intro-text">
                <h1 class="uk-text-center">Inovação e modernização para os fios</h1>
                <p class="uk-text-center">Mude, inove, transforme, use Anabelle</p>
                <hr>
            </div>
        </div>
    </div>
</section>

<!-- Produtos -->
<section id="produtos">
    <div class="uk-section produtos">
        <div class="uk-container">
            <div uk-filter="target: .js-filter">
                <a class="uk-button uk-button-default" href="" uk-icon="chevron-down">Categorias</a>
                <div uk-dropdown="mode: click">
                    <ul class="uk-nav uk-dropdown-nav">
                        <li class="uk-active" uk-filter-control><a href="#">Todos os Produtos</a></li>
                        <li uk-filter-control=".reducao-volume"><a href="#">Redução de Volume</a></li>
                        <li uk-filter-control=".tratamento"><a href="#">Tratamento</a></li>
                        <li uk-filter-control=".coloracao"><a href="#">Coloração</a></li>
                        <li uk-filter-control=".home-care"><a href="#">Home Care</a></li>
                        <li uk-filter-control=".lavatorio"><a href="#">Lavatório</a></li>
                    </ul>
                </div>
                <ul class="js-filter uk-child-width-1-1 uk-child-width-1-3@m uk-text-center block-products" uk-grid>
                    <li class="home-care">
                        <div class="uk-card">
                           <a href="products/treat-hair-blond-shampoo">Treat Hair Blond Shampoo <img src="../assets/image/thumbs/treat-hair-blond-shampoo.png" alt="treat-hair-blond-shampoo"></a>
                        </div>
                    </li>
                    <li class="home-care">
                        <div class="uk-card">
                            <a href="products/treat-hair-blond-condicionador">Treat Hair Blond Condicionador <img src="../assets/image/thumbs/treat-hair-blond-condicionador.png" alt="treat-hair-blond-condicionador"></a>
                        </div>
                    </li>
                    <li class="home-care">
                        <div class="uk-card">
                            <a href="products/treat-hair-blond-mascara">Treat Hair Blond Máscara <img src="../assets/image/thumbs/treat-hair-blond-mascara.png" alt="treat-hair-blond-mascara"></a>
                        </div>
                    </li>
                    <li class="home-care">
                        <div class="uk-card">
                            <a href="products/perfect-waves-shampoo">Perfect Waves Shampoo <img src="../assets/image/thumbs/perfect-waves-shampoo.png" alt="perfect-waves-shampoo"></a>
                        </div>
                    </li>
                    <li class="home-care">
                        <div class="uk-card">
                            <a href="products/perfect-waves-condicionador">Perfect Waves Condicionador <img src="../assets/image/thumbs/perfect-waves-condicionador.png" alt="perfect-waves-condicionador"></a>
                        </div>
                    </li>
                    <li class="home-care">
                        <div class="uk-card">
                            <a href="products/perfect-waves-ativador-de-cachos">Perfect Waves Ativador de Cachos <img src="../assets/image/thumbs/perfect-waves-ativador-de-cachos.png" alt="perfect-waves-ativador-cachos"></a>
                        </div>
                    </li>
                    <li class="home-care">
                        <div class="uk-card">
                            <a href="products/moringa-oil">Moringa Oil  <img src="../assets/image/thumbs/moringa-oil.png" alt="moringa-oil"></a>
                        </div>
                    </li>
                    <li class="lavatorio">
                        <div class="uk-card">
                            <a href="products/mascara-hidratante">Máscara Hidratante <img src="../assets/image/thumbs/mascara-hidratante.png" alt="mascara-hidratante"></a>
                        </div>
                    </li>
                    <li class="lavatorio">
                        <div class="uk-card">
                            <a href="products/shampoo-hidratante">Shampoo Hidratante <img src="../assets/image/thumbs/shampoo-hidratante.png" alt="shampoo-hidratante"></a>
                        </div>
                    </li>
                    <li class="lavatorio">
                        <div class="uk-card">
                            <a href="products/condicionador-hidratante">Condicionador Hidratante <img src="../assets/image/thumbs/condicionador-hidratante.png" alt="condicionador-hidratante"></a>
                        </div>
                    </li>
                </ul>
            </div>
            <br> <br>
            <ul class="uk-pagination uk-flex-center" uk-margin>
                <li class="border-pagination"><a href="#"><span>pag 3 de 3</span></a></li>
                <li><a href="products">1</a></li>
                <li><a href="products/2">2</a></li>
                <li><a href="products/3">3</a></li>
                <li><a href="#"><span uk-pagination-next></span></a></li>
            </ul>
        </div>
    </div>
</section> 
<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/tratamento-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/product-page/absolut-pro-condicionador.png" alt="absolut-pro-condicionador">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">treat hair absolut pro condicionador</h2>
                        <hr>
                        <p>Com uma forma única rica em polímeros hidratantes, condiciona e hidrata o cabelo, deixando o macio e fácil de pentear.</p>

                        <h5>Volume</h5>
                        <p>1 Litro</p>

                        <h5>Precauções</h5>
                        <p>Mantenha em local fresco ao abrigo de luz. Mantenha fora do alcance de crianças, em caso de contato com os olhos, lave abundantemente com água.</p>

                        <h5>Modo de Usar</h5>
                        <p>Aplique o produto em quantidade suficiente com os cabelos já lavados, massageie da raiz para as pontas, deixe agir por alguns minutos, enxague em seguida.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua – Cetearyl Alcohol – Glycerin – Cetearyl Alcohol/ Behentrimonium Methosulfate – PEG-40 / PPG- 8 Methylaminopropyl / Hydropropyl Dimethicone Copolymer – Polyquaternium-7-Cetrimonium Chloride – Behentrimonium Chloride – Stearamidopropyl Dimethylamine – CL 19140 – Parfum : Butylphenyl Methylpropional / Citral / Hexyl Cinnamal / Limonene/ Linalool / Methyl 2 – Octynoate Latic Acid – Cyclomethicone / Dimethicone – Butyrospermum ParkII (Shea) Butter – Hydroxyethycellulose – CI 15985 – Dissodium EDTA – Methylchloroiso; Hiazolinone / Methyllisothiazolinone/ Magnesium Chloride/ Magnesium Nitrate – BHT.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/moringa-oil">
                        <img src="../assets/image/thumbs/moringa-oil.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">moringa oil</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-mascara">
                        <img src="../assets/image/thumbs/mini-absolut-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-plus-intensive-mask">
                        <img src="../assets/image/thumbs/treat-hair-plus-intensive-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair plus intensive mask</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/mascara-hidratante">
                        <img src="../assets/image/thumbs/mascara-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">máscara hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-shampoo">
                        <img src="../assets/image/thumbs/perfect-waves-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-sinergica-blond">
                        <img src="../assets/image/thumbs/mini-supra-blond.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra sinérgica blond</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-day-protector">
                        <img src="../assets/image/thumbs/mini-supra-day-protector.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra day protector</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
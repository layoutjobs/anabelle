<!-- Banner Topo Produtos -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/produtos.png); height: 100%;">
            <div class="intro-text uk-padding-large">
                <h1 class="uk-text-center">Inovação e modernização para os fios</h1>
                <p class="uk-text-center">Mude, inove, transforme, use Anabelle</p>
                <hr>
            </div>
        </div>
    </div>
</section>

<!-- Produtos -->
<section id="produtos">
    <div class="uk-section produtos">
        <div class="uk-container">
            <div uk-filter="target: .js-filter">
                <a class="uk-button uk-button-default" href="" uk-icon="chevron-down">Categorias</a>
                <div uk-dropdown="mode: click">
                    <ul class="uk-nav uk-dropdown-nav">
                        <li class="uk-active" uk-filter-control><a href="#">Todos os Produtos</a></li>
                        <li uk-filter-control=".reducao-volume"><a href="#">Redução de Volume</a></li>
                        <li uk-filter-control=".tratamento"><a href="#">Tratamento</a></li>
                        <li uk-filter-control=".coloracao"><a href="">Coloração</a></li>
                        <li uk-filter-control=".home-care"><a href="">Home Care</a></li>
                        <li uk-filter-control=".lavatorio"><a href="">Lavatório</a></li>
                    </ul>
                </div>
                <ul class="js-filter uk-child-width-1-1 uk-child-width-1-3@m uk-text-center block-products" uk-grid>
                    <li class="home-care">
                        <div class="uk-card">
                            <a href="products/treat-hair-absolut-condicionador">Treat Hair Absolut Condicionador<img src="../assets/image/thumbs/mini-absolut-condicionador.png" alt="mini-absolut-condicionador"></a>
                        </div>
                    </li>
                    <li class="home-care">
                        <div class="uk-card">
                            <a href="products/treat-hair-absolut-shampoo">Treat Hair Absolut Shampoo <img src="../assets/image/thumbs/mini-absolut-shampoo.png" alt="mini-absolut-shampoo"></a>
                        </div>
                    </li>
                    <li class="home-care">
                        <div class="uk-card">
                            <a href="products/treat-hair-absolut-leave-in">Treat Hair Absolut Leave-in <img src="../assets/image/thumbs/mini-absolut-leave-in.png" alt="mini-absolut-leave-in"></a>
                        </div>
                    </li>
                    <li class="home-care">
                        <div class="uk-card">
                            <a href="products/treat-hair-absolut-mascara">Treat Hair Absolut Mascara <img src="../assets/image/thumbs/mini-absolut-mask.png" alt="mini-absolut-mask"></a>
                        </div>
                    </li>
                    <li class="reducao-volume">
                        <div class="uk-card">
                            <a href="products/supra-sinergico-expert">Supra Sinérgico Expert <img src="../assets/image/thumbs/mini-supra.png" alt="supra-sinergico-expert"></a>
                        </div>
                    </li>
                    <li class="tratamento">
                        <div class="uk-card">
                            <a href="products/supra-day-protector">Supra Day Protector <img src="../assets/image/thumbs/mini-supra-day-protector.png" alt="supra-day-protector"></a>
                        </div>
                    </li>
                    <li class="tratamento">
                        <div class="uk-card">
                            <a href="products/treat-hair-plus-capsulas">Treat Hair Plus Cápsulas <img src="../assets/image/thumbs/mini-treat-hair-plus-capsula.png" alt="treat-hair-capsula"></a>
                        </div>
                    </li>
                    <li class="tratamento">
                        <div class="uk-card">
                            <a href="products/treat-hair-absolute-pro-shampoo">Treat Hair Absolut Pro Shampoo <img src="../assets/image/thumbs/mini-absolut-pro-shampoo.png" alt="treat-hair-absolut-pro-shampoo"></a>
                        </div>
                    </li>
                    <li class="tratamento">
                        <div class="uk-card">
                            <a href="products/treat-hair-absolute-professional-condicionador">Treat Hair Absolut Pro Condicionador <img src="../assets/image/thumbs/mini-absolut-pro-condicionador.png" alt="treat-hair-absolut-condicionador"></a>
                        </div>
                    </li>
                </ul>
            </div>
            <br> <br>
            <ul class="uk-pagination uk-flex-center" uk-margin>
                <li class="border-pagination"><a href="#"><span>pag 1 de 3</span></a></li>
                <li><a href="products">1</a></li>
                <li><a href="products/2">2</a></li>
                <li><a href="products/3">3</a></li>
                <li><a href="#"><span uk-pagination-next></span></a></li>
            </ul>
        </div>
    </div>
</section> 
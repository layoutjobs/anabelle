<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(../assets/image/banners/home-care-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="../assets/image/product-page/perfect-waves-ativador-de-cachos.png" alt="perfect-waves-ativador-cachos">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">perfect waves ativador de cachos</h2>
                        <hr>
                        <p>Um produto com fórmula exclusiva enriquecido com Pantenol (vitamina B5) e óleo de abacate (Avocado). Promove hidratação, umectação e nutrição nos fios, deixando-os com cachos perfeitos.</p>

                        <h5>Volume</h5>
                        <p>250ml</p>

                        <h5>Precauções</h5>
                        <p>Mantenha em local fresco ao abrigo de luz, mantenha fora do alcance de crianças, em caso de contato com os olhos, lave abundantemente com água.</p>

                        <h5>Modo de Usar</h5>
                        <p>Após lavar os cabelos com a Linha Perfect Waves, com os cabelos úmidos aplicar uma pequena quantidade do Ativador de Cachos Perfect Waves em toda a extensão dos fios.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua, ceteayl alcohol, glycerin, petrolatum, linum usitatissimum (linseed) seed extract/salvia hispânica seed, extract, cetearyl alcohol/behentrimonium methosulfate, cetrimonium chloride, propylene glycol, stearamidopropyl dimethylamine, lactic acid, persea gratissima oil, parfum: Benzyl alcohol/benzyl salicylate/hydroxycitronellal/hydroxyisohe xyl3, cyclohexene carboxaldehyde/limonene/linalool, phenyl trimethicone, panthenol, methylchloroisothiazolinone/methylisothiazolinone/magnesium chloride/magnesium nitrate, disodium edta.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/treat-hair-absolut-leave-in">
                        <img src="../assets/image/thumbs/mini-absolut-leave-in.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">treat hair absolut leave-in</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/moringa-oil">
                        <img src="../assets/image/thumbs/moringa-oil.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">moringa oil</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-mascara">
                        <img src="../assets/image/thumbs/mini-absolut-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/mascara-hidratante">
                        <img src="../assets/image/thumbs/mascara-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">máscara hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/titanium-colors-caixa">
                        <img src="../assets/image/thumbs/mini-titanium-colors.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">titanium colors</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-shampoo">
                        <img src="../assets/image/thumbs/mini-absolut-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-liss-pro-redutor-volume">
                        <img src="../assets/image/thumbs/treat-liss-pro-redutor-de-volume.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat liss pro redutor de volume</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
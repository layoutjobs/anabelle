<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/home-care-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/product-page/absolut-shampoo.png" alt="absolut-shampoo">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">Treat Hair – Shampoo Absolut</h2>
                        <hr>
                        <p>Um produto com fórmula exclusiva proporciona uma limpeza com um toque de suavidade e maciez.</p>

                        <h5>Volume</h5>
                        <p>250ml | 8,45 fl Oz</p>

                        <h5>Precauções</h5>
                        <p>Mantenha em local fresco ao abrigo de luz. Mantenha fora do alcance de crianças, em caso de contato com os olhos, lave abundantemente com água. </p>

                        <h5>Modo de Usar</h5>
                        <p>Aplique o shampoo Treat Absolut nos cabelos úmidos, massageando por dois minutos, enxague e repita o processo.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua – Sodium Laureth Sulfate-Amonium Laureth-12, Sulfate – Cocamidopropyl Betaine – Sodium Cocoamphoacetate – Glycol Distearate / Sodium Laureth Sulfate / Cocamidopropyl Betaine – Cocamide Mipa – CI19140 – Parfum: Butylphenyl Methylpropional/Cital/Hexyl Cinnamal/ Limonene; Linalool/ Methyl 2 – Octynoate. Glycol Distearate – Peg – 120 Methyl Glucose Trioleate / Propylene Glycol – PEG-40 PPg-8 Methylaminopropyl/ Hydropropyl Dimethicone Copolymer – CI 15985 – Polyquaternium – 67 – Guar Hydroxypropyltrimonium Chloride – PEG-12 Dimethicone – Methylchloroisothiazolinone / Methylisothiazolinone / Magnesium Chloride / Magnesium Nitrate – Disodium EDTA – Lactic Acid.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/condicionador-hidratante">
                        <img src="assets/image/thumbs/condicionador-hidratante.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">condicionador hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/moringa-oil">
                        <img src="assets/image/thumbs/moringa-oil.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">moringa oil</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-sinergica-expert">
                        <img src="assets/image/thumbs/mini-supra.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra sinérgico expert</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/mascara-hidratante">
                        <img src="assets/image/thumbs/mascara-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">máscara hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-blond-shampoo">
                        <img src="assets/image/thumbs/treat-hair-blond-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair blond shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-plus-capsulas">
                        <img src="assets/image/thumbs/mini-treat-hair-plus-capsula.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair plus cápsulas</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-liss-pro-shampoo">
                        <img src="assets/image/thumbs/treat-liss-pro-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat liss pro shampoo</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
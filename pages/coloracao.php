<!-- BannerTopo -->
<section id="banner-topo-colors">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/coloracao.png); height: 700px;">
            <div class="intro-text uk-padding-large">
                <h1>Crie transformações, aposte nas cores!</h1>
                <p>Titanium Colors - coloração com tecnologia Angstrom</p>
                <a class="uk-button uk-button-default" href="#amostragem-cores" uk-scroll>CORES</a>
            </div>
        </div>
    </div>
</section>

<!-- Main -->
<section id="coloracao">
    <div class="uk-section coloracao">
        <div class="uk-container">
            <h2 class="uk-text-center">Titanium Colors</h2>
            <hr>
            <p class="uk-text-center">A linha Titanium Colors da Anabelle Cosméticos utiliza a tecnologia alemã Agstrom e o Blend 10 Oil para trazer alta qualidade na pigmentação, junto a um menor percentual de amônia. O resultado é uma linha completa com coloração sofisticada, eficaz e duradoura!</p>

            <div class="block-text">
                <div class="uk-grid-large uk-child-width-expand@l uk-text-center" uk-grid>
                    <div>
                        <div class="uk-card">
                            <img src="assets/image/icone-coloracao-1.png" alt="icone-coloracao">
                            <h4>Agstrom</h4>
                            <p>Com ativos 10 vezes menores que na nano tecnologia, permitindo assim, que os pigmentos sejam reduzidos e mais eficientes. </p>
                        </div>
                    </div>

                    <div>
                        <div class="uk-card">
                            <img src="assets/image/icone-coloracao-2.png" alt="icone-coloracao">
                            <h4>Cores intensas</h4>
                            <p>A alta tecnologia e a qualidade da pigmentação, garantem cores intensas para uma linha completa. </p>
                        </div>
                    </div>

                    <div>
                        <div class="uk-card">
                            <img src="assets/image/icone-coloracao-3.png" alt="icone-coloracao">
                            <h4>Blend 10 Oil</h4>
                            <p>O Blend 10 Oil eleva a qualidade da pigmentação, hidratando os fios na hora da transformação!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Cores -->
<section id="amostragem-cores">
    <div class="uk-section amostragem-cores">
        <div class="uk-container">
            <img class="titanium-colors" src="assets/image/img-titanium-color.png" alt="titanium-colors">
            <br>
            <h2 class="uk-text-center">Cores Disponíveis</h2>
            <hr>

            <div class="block-colors">
                <div class="uk-grid-large uk-child-width-expand@s uk-text-center" uk-grid>
                    <div>
                        <div class="uk-card">
                            <img src="assets/image/mostruario-naturais.png" alt="mostruario-natural">
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <img src="assets/image/mostruario-vermelho-especial.png" alt="mostruario-vermelho-natural">
                        </div>
                    </div>
                </div>
                <div class="uk-grid-large uk-child-width-expand@s uk-text-center" uk-grid>
                    <div>
                        <div class="uk-card">
                            <img src="assets/image/mostruario-serie-titanium.png" alt="mostruario-titanium">
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <img src="assets/image/mostruario-perolas.png" alt="mostruario-perolas">
                        </div>
                    </div>
                </div>

                <div class="uk-grid-large uk-child-width-expand@s uk-text-center" uk-grid>
                    <div>
                        <div class="uk-card">
                            <img src="assets/image/mostruario-marrom-intensos.png" alt="mostruario-marrom-intenso">
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <img src="assets/image/mostruario-intensificadores.png" alt="mostruario-intensificadores">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Estrela de Oswald -->
<section id="estrela-oswald">
    <div class="uk-section estrela-oswald">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l uk-text-center" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/tabela-cores.png" alt="tabela-cores">
                    </div>
                </div>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/estrela-oswald.png" alt="estrela-oswald">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
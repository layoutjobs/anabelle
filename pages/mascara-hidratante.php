<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/lavatorio-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/product-page/mascara-hidratante.png" alt="mascara-hidratante">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">máscara hidratante</h2>
                        <hr>
                        <p>Desenvolvida com extrato de mandioca e silicones AMINOFUNCIONAL, a máscara hidratante de 2KG da Anabelle Cosméticos, é rica em proteínas, vitaminas A e C e minerais, como ferro, cálcio e fósforo, elementos essenciais para a nutrição da fibra capilar. Promove hidratação, nutrição profunda e fortalecimento dos fios. Auxilia no crescimento dos cabelos e ajuda no alinhamento das cutículas, proporcionando, brilho intenso, leveza e sedosidade.</p>

                        <h5>Volume</h5>
                        <p>2kg</p>
                        

                        <h5>Precauções</h5>
                        <p>Manter fora do alcance de crianças, em caso de irritação, suspenda o uso. Em caso de contato com os olhos, lave-os abundantemente.</p>

                        <h5>Modo de Usar</h5>
                        <p>Lave os cabelos com o shampoo, retire o excesso de umidade, coloque a quantidade desejada na palma da mão, aplique de maneira uniforme nos cabelos, deixe agir de 2 a 5 minutos, enxague bem e penteie.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>AQUA, CETEARYL ALCOHOL, GLYCERIN, CETRIMONIUM CHLORIDE, GLYCERYL STEARATE, BEHENTRIMONIUM CHLORIDE, STEARAMIDOPROPRYL DIMETHYLAMINE, CYCLOPENTASILOXANE/DIMETHICONE, LACTIC ACID, PARFUM: BENZYL SALICYLATE/COUMARIN/HEXYL CINNAMAL/LOMONENE/LINALOOL, BUTYROSPERFMUM PARKIL BUTTER, PERSEA GRATISSIMA OIL, MANIHOT UTILÍSSIMA ROOT FIBER, METHYLCHLOROISOTHIAZOLINONE/ METHYLSOTHIAZOLINONE, DISODIUMEDTA, BHT.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/ox-titanium-colors">
                        <img src="assets/image/thumbs/mini-ox-titanium.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">ox titanium colors</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/black-platinum-progressive-brush">
                        <img src="assets/image/thumbs/black-platinum.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">black platinum</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-plus-intensive-mask">
                        <img src="assets/image/thumbs/treat-hair-plus-intensive-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair plus intensive mask</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-sinergico-expert">
                        <img src="assets/image/thumbs/mini-supra.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra sinérgico expert</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-light-po-descolorante">
                        <img src="assets/image/thumbs/mini-po-descolorante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">pó descolorante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-day-protector">
                        <img src="assets/image/thumbs/mini-supra-day-protector.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra day protector</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-ativador-de-cachos">
                        <img src="assets/image/thumbs/perfect-waves-ativador-de-cachos.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves ativador de cachos</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/home-care-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/product-page/treat-hair-blond-shampoo.png" alt="treat-hair-blond-shampoo">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">treat hair blond shampoo</h2>
                        <hr>
                        <p>Um produto com fórmula exclusiva que proporciona limpeza delicada. Enriquecido com pigmentos violetas selecionados, atua como agentes neutralizantes, reduzindo os tons amarelados indesejados dos cabelos loiros ou com mechas, e a creatina, que ajuda na recuperação dos fios loiros e descoloridos.</p>

                        <h5>Volume</h5>
                        <p>300ml</p>

                        <h5>Precauções</h5>
                        <p>Mantenha em local fresco ao abrigo de luz. Mantenha fora do alcance de crianças, em caso de contato com os olhos, lave abundantemente com água.</p>

                        <h5>Modo de Usar</h5>
                        <p>Aplique o Shampoo Blond Treat Hair nos cabelos úmidos, massageando por dois minutos, enxague e repita o processo.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua, Sodium Laureth Sulfate, Cocoamide Dea, Glycol Distearate / Sodium Laureth Sulfate/ Cocamidopropyl Betaine; Decyl Glucoside; Sodium Chloride; Parfum; Butylphenylmethylpropional/ Benzyl Salicylate/ Citral/ Coumarin/ D-Limonene/ Geraniol/ Linalool; DMDM Hydantoin; Disodium Edta; CI 60.730 CI 20.470; Citric Acid.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/perfect-waves-condicionador">
                        <img src="assets/image/thumbs/perfect-waves-condicionador.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">perfect waves condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-plus-capsulas">
                        <img src="assets/image/thumbs/mini-treat-hair-plus-capsula.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair plus cápsulas</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-professional-mascara">
                        <img src="assets/image/thumbs/mini-absolut-pro-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut pro máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/ox-titanium-colors">
                        <img src="assets/image/thumbs/mini-ox-titanium.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">ox titanium colors</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-sinergico-expert">
                        <img src="assets/image/thumbs/mini-supra.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra sinérgico expert</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/black-platinum-progressive-brush">
                        <img src="assets/image/thumbs/black-platinum.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">black platinum</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-pro-shampoo">
                        <img src="assets/image/thumbs/mini-absolut-pro-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut pro shampoo</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
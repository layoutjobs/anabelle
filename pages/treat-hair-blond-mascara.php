<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/home-care-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/product-page/treat-hair-blond-mascara.png" alt="produto-teste">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">treat hair blond máscara</h2>
                        <hr>
                        <p>Um produto com fórmula exclusiva enriquecida com pigmentos violetas selecionados e creatina, atuam como agentes neutralizantes, reduzindo os tons amarelados indesejados dos cabelos loiros ou com mechas, e, creatina, que ajuda na recuperação dos fios loiros e descoloridos.</p>

                        <h5>Volume</h5>
                        <p>250g</p>

                        <h5>Precauções</h5>
                        <p>Mantenha em local fresco ao abrigo de luz, mantenha fora do alcance de crianças. Em caso de contato com os olhos, lave abundantemente com água.</p>

                        <h5>Modo de Usar</h5>
                        <p>Após o uso do Shampoo Blond Treat Hair, com os cabelos úmidos, aplique uma pequena quantidade da Máscara Blond Treat Hair em toda a extensão dos fios, massageando e fazendo um controle visual até alcançar o tom desejado. Enxague em seguida.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>AQUA, PARFUM, ACID VIOLET 43, CYCLOMETHICONE, DIMETHICONE, PHENYLTRIMETICONE, TRIMETHYLSILYLAMODIMETHICONE, HYDROLIZATED KERATIN, AMODIMETHYCONE, CETRIMONIUM CHLORIDE, DISSODIUM EDTA, CETYL ALCOHOL, METHYLPARABENPROPYPARABEN, PROPYLENEGLYCOL, PARAFFINLIQUID, ISOPROPYLPALMITATE, CITRIC ACID, CREATINE. </p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/treat-hair-absolut-mascara">
                        <img src="assets/image/thumbs/mini-absolut-mask.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">treat hair absolut máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-professional-mascara">
                        <img src="assets/image/thumbs/mini-absolut-pro-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut pro máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-liss-pro-redutor-volume">
                        <img src="assets/image/thumbs/treat-liss-pro-redutor-de-volume.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat liss pro redutor de volume</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-ativador-de-cachos">
                        <img src="assets/image/thumbs/perfect-waves-ativador-de-cachos.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves ativador de cachos</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/condicionador-hidratante">
                        <img src="assets/image/thumbs/condicionador-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">condicionador hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/mascara-hidratante">
                        <img src="assets/image/thumbs/mascara-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">máscara hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-blond-shampoo">
                        <img src="assets/image/thumbs/treat-hair-blond-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair blond shampoo</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
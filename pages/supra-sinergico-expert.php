<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/reducao-de-volume-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/product-page/supra.png" alt="supra-sinergico-expert">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">supra sinérgico expert</h2>
                        <hr>
                        <p>A progressiva Supra Sinérgica Expert foi desenvolvida com uma fórmula exclusiva. Composta pelos aminoácidos mais nobres e pelos ácidos láctico, cítrico e hialurônico. Sem formol em sua composição, o Supra Sinérgico age na fibra capilar desligando temporariamente as pontes de enxofre e depositando aminoácidos. Outro componente do Supra Sinérgico que auxilia no cuidado com os fios durante o processo de alisamento, é a Sericina, uma proteína extraída da seda que, neste caso, atua como seladora de cutículas, assim, enquanto o ácido hialurônico preenche as falhas nos fios, devolvendo-lhes a massa perdida, a sericina forma um filme em volta deles, fortalecendo a fibra capilar e garantindo-lhe sedosidade.</p>

                        <h5>Volume</h5>
                        <p>1 Litro</p>

                        <h5>Precauções</h5>
                        <p>Mantenha armazenado em local seco protegido da luz e calor, fora do alcance de crianças. Produto de uso externo. Uso exclusivo profissional. Não aplique o produto se o couro cabeludo estiver irritado ou lesionado.</p>

                        <h5>Modo de Usar</h5>
                        <p>Dividir o cabelo em quadrantes, com o auxílio de um pincel aplicar o restaurador Supra Sinérgico no cabelo seco, deixar agir de 50 minutos a 1 hora e 20 minutos conforme o objetivo desejado, enxaguar aplicar uma pequena quantidade do termo protetor supra day protector, em toda extensão dos fios escovar e pranchar.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>AQUA, GLYCERIN, CETEARYL ALCOOHOL, BEHENTRIMONIUM CHLORIDE, CETRIMONIUM CHLORIDE, BUTYROSPERMUM PARKII BUTTER, PARFUM: BUTYLPHENYL METHYLPROPIONAL/BENZYL BENZOATE/BENZYL SALICYLATE/ COUMARIN / LIMONENE / HEXYL CINNAMAL / HYDROXYISOHEXYL 3-CYCLOHEXENECARBOXALDEHYDE / LINALOOL PEG-40 / PPG-8 METHYLAMINOPROPYL / HYDROPROPYL DIMETHICONE COPOLYMER, CETYL ACETATE / ACETYLATED LANOLIN ALCOHOL, HYDROXYETHYLCELLULOSE, PHENYL TRIMETHICONE, STEARAMIDOPROPYL DIMETHYLAMINE, PHOSPHORIC ACID DISSODIUM EDTA, METHYLCHLOROISOTHIAZOLINONE / METHYLLISOTHIAZOLINONE / MAGNESIUM CHLORIDE / MAGNESIUM NITRATE, LACTIC ACID, SILK AMINO ACIDS, GUAR HYDROXYPROPYLTRIMONIUM CHLORIDE. </p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/perfect-waves-ativador-de-cachos">
                        <img src="assets/image/thumbs/perfect-waves-ativador-de-cachos.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">perfect waves ativador de cachos</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-mascara">
                        <img src="assets/image/thumbs/mini-absolut-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut pro máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/shampoo-hidratante">
                        <img src="assets/image/thumbs/shampoo-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">shampoo hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/black-platinum-progressive-brush">
                        <img src="assets/image/thumbs/black-platinum.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">black platinum</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/mascara-hidratante">
                        <img src="assets/image/thumbs/mascara-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">mascara hidrante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/moringa-oil">
                        <img src="assets/image/thumbs/moringa-oil.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">moringa oil</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-day-protector">
                        <img src="assets/image/thumbs/mini-supra-day-protector.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra day protector</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
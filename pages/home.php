<!-- Banner Home -->
<section id="banner-topo">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/home.png); height: 700px;">
            <div class="intro-text uk-padding-small">
                <h1>A transformação da beleza dos fios</h1>
                <p>cabelos bem cuidados, com saúde e esplendor</p>
                <a class="uk-button uk-button-default" href="produtos">PRODUTOS</a>
            </div>
        </div>
    </div>
</section>

<!-- Quem Somos? -->
<section id="quem-somos">
    <div class="uk-section quem-somos">
        <div class="uk-container">
            <h2 class="uk-text-center">Quem Somos</h2>
            <hr>
            <p class="uk-text-center">A Anabelle Cosméticos celebra a beleza com uma linha completa de produtos profissionais para cabelos, levando inovação e modernização aos salões de beleza do Brasil.</p>
            <div class="block-text">
                <div class="uk-grid-large uk-child-width-expand@l uk-text-center" uk-grid>
                    <div>
                        <div class="uk-card">
                            <img src="assets/image/icones-home-1.png" alt="qualidade-profissional">
                            <h4>QUALIDADE PROFISSIONAL</h4>
                            <p>São diversos produtos com qualidade e feitos especialmente para os melhores profissionais do mercado.</p>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <img src="assets/image/icones-home-2.png" alt="inovacao-tecnologia">
                            <h4>INOVAÇÃO E TECNOLOGIA</h4>
                            <p>As melhores inovações do mercado aplicadas em produtos através de processos e análises tecnológicas. </p>
                        </div>
                    </div>
                    <div>
                        <div class="uk-card">
                            <img src="assets/image/icones-home-3.png" alt="resultado-comprovado">
                            <h4>RESULTADO COMPROVADO</h4>
                            <p>A eficácia dos produtos é comprovada com fios com mais suavidade, brilho e maciez.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Diferencial Anabelle -->
<section id="diferencial-anabelle">
    <div class="uk-section diferencial">
        <div class="uk-container">
            <h2 class="uk-text-center">Diferencial Anabelle</h2>
            <hr>
            <p class="uk-text-center">Uma linha completa com inovação e tecnologia, que se destaca por levar beleza aos fios com produtos que passam pela manutenção de fios, linha restauradora e coloração. </p>
        </div>
    </div>
</section>


<!-- Categoria Anabelle -->
<section id="categoria-produto">
    <div class="uk-section categoria">
        <div class="uk-container">
            <!-- CardLeft -->
            <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s" uk-grid>
                <div class="uk-card-media-left uk-cover-container">
                    <img src="assets/image/banners/banner-box3.png" alt="banner-box-1" uk-cover>
                    <canvas width="600" height="400"></canvas>
                </div>
                <div>
                    <div class="uk-card-body uk-text-position">
                        <h3 class="uk-card-title uk-text-center">Cores e Tons Especiais</h3>
                        <hr>
                        <p class="uk-text-center">A linha Titanium Colors traz a surpreendente tecnologia Angstrom e o Blend 10 Oil para criar pigmentos sofisticados com cores mais duradouras e o brilho que só a Anabelle é capaz de proporcionar!</p>
                    </div>
                </div>
            </div>

            <!-- CardRight -->
            <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s" uk-grid>
                <div class="uk-flex-last@s uk-card-media-right uk-cover-container">
                    <img src="assets/image/banners/banner-box2.png" alt="banner-box-2" uk-cover>
                    <canvas width="600" height="400"></canvas>
                </div>
                <div>
                    <div class="uk-card-body uk-text-position">
                        <h3 class="uk-card-title uk-text-center">cabelos saudáveis</h3>
                        <hr>
                        <p class="uk-text-center">Uma linha completa que prioriza a saúde e qualidade dos fios! Levando beleza, maciez, brilho e sedosidade para todos os tipos de fios. É a alta qualidade acessível para todos os salões de beleza.</p>
                    </div>
                </div>
            </div>

            <!-- CardLeft -->
            <div class="uk-card uk-card-default uk-grid-collapse uk-child-width-1-2@s" uk-grid>
                <div class="uk-card-media-left uk-cover-container">
                    <img src="assets/image/banners/banner-box1.png" alt="banner-box-3" uk-cover>
                    <canvas width="600" height="400"></canvas>
                </div>
                <div>
                    <div class="uk-card-body uk-text-position">
                        <h3 class="uk-card-title uk-text-center">tratamento profissional</h3>
                        <hr>
                        <p class="uk-text-center">A Anabelle Cosméticos foi criada especialmente para atender aos melhores salões do Brasil. São produtos feitos exclusivamente para profissionais com inovação e tecnologia. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> 
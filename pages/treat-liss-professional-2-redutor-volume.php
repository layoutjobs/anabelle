<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/reducao-de-volume-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/product-page/treat-liss-pro-redutor-de-volume.png" alt="treat-liss-pro-redutor-volume">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">treat liss pro redutor de volume</h2>
                        <hr>
                        <p>O Redutor de Volume Treat Liss é rico em Óleo de Ojon e nano partículas de tratamento. Foi desenvolvida para reduzir o volume e tratar a fibra capilar. Com alto poder de alisamento, ela também alinha firmemente as cutículas e corrige a porosidade deixando os cabelos macios, sedosos e com brilho. Sua fórmula é de fácil aplicação.</p>

                        <h5>Volume</h5>
                        <p>1 Litro</p>

                        <h5>Precauções</h5>
                        <p>Evitar o couro cabeludo. Manter fora do alcance de crianças. Em caso de contato com os olhos, enxaguar abundantemente, havendo algum tipo de irritação, procurar orientação médica.</p>

                        <h5>Modo de Usar</h5>
                        <p>Após a lavagem dos cabelos com o shampoo, aplicar o Redutor de Volume e deixar agir 15 minutos, tirar o excesso com uma toalha e escovar. Pranchar 15 vezes cada mecha fina. Usar um creme após o processo e enxaguar. </p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Water/aqua, Propyleneglicol, Cetrimonium cloryde, Trietanolamina, Dissodium EDTA, BHT, Polyquaternium 10, Cetearyl alcohol, Ceteareth 20, Mineral oil, Sweetalmond oil, Dimethicone and ciclomethycone, Keratin, Glioxilic acid, Polyquaternium 7, Polyquaternium 10, Polyquarternium 33, Fragrance parfum. </p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/treat-hair-absolut-condicionador">
                        <img src="assets/image/thumbs/mini-absolut-condicionador.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">treat hair absolut condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-light-po-descolorante">
                        <img src="assets/image/thumbs/mini-po-descolorante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">pó descolorante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/moringa-oil">
                        <img src="assets/image/thumbs/moringa-oil.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">moringa oil</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/ox-titanium-colors">
                        <img src="assets/image/thumbs/mini-ox-titanium.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">ox titanium colors</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/mascara-hidratante">
                        <img src="assets/image/thumbs/mascara-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">máscara hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/black-platinum-progressive-brush">
                        <img src="assets/image/thumbs/black-platinum.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">black platinum</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-ativador-de-cachos">
                        <img src="assets/image/thumbs/perfect-waves-ativador-de-cachos.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves ativador de cachos</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/home-care-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/product-page/absolut-mask.png" alt="absolut-mask">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">treat hair absolut máscara</h2>
                        <hr>
                        <p>Com uma composição rica em ativos hidratantes, trata os fios, deixando-os com um aspecto muito saudável com bastante brilho e emoliência.</p>

                        <h5>Tamanhos</h5>
                        <p>250g | 8.4 fl Oz</p>

                        <h5>Precauções</h5>
                        <p>Mantenha em local fresco ao abrigo de luz, mantenha fora do alcance de crianças, em caso de contato com os olhos, lave abundantemente com água.</p>

                        <h5>Modo de Usar</h5>
                        <p>Aplique o produto em quantidade suficiente com os cabelos já lavados, massageie da raiz para as pontas, deixe agir de 10 a 15 minutos, enxague em seguida. </p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>AQUA – CETEARYL ALCOHOL – GLYCERIN – BEHENTRIMONIUM CHLORIDE – OZOKERITE – CETRIMONIUM CHLORIDE – PEG-40/PPG-8 METHYLAMINOPROPYL/ HYDROPROPYL DIMETHICONE COPOLYMER – CI19140 – STEARAMIDOPROPYL DIMETHYLAMINE – PARFIM: BUTYLPHENYL METHYLPROPIONAL/CITRAL/HEXYL CINNAMAL/ LIMONENE/ LINALOOL/ METHYL 2 – OCTYNOATE. LACTIC ACID – MINERAL OIL- SODIUM PCA- BUTYROSPERMUM PARKII BUTTER – CI25985 – DISSODIUM EDTA – METHYLCHLOROISOTHIAZOLINONE/ METHYLISOTHIAZOLINONE/ MAGNESIUM CHLORIDE/ MAGNESIUM NITRATE – BHT.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/treat-hair-absolut-pro-shampoo">
                        <img src="assets/image/thumbs/treat-liss-pro-shampoo.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">treat liss pro shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-sinergica-blond">
                        <img src="assets/image/thumbs/mini-supra-blond.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra sinérgico blond</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-condicionador">
                        <img src="assets/image/thumbs/perfect-waves-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-liss-pro-redutor-volume">
                        <img src="assets/image/thumbs/treat-liss-pro-redutor-de-volume.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat liss pro redutor de volume</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-professional-condicionador">
                        <img src="assets/image/thumbs/mini-absolut-pro-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut pro condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-plus-intensive-mask">
                        <img src="assets/image/thumbs/treat-hair-plus-intensive-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair plus intensive mask</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-day-protector">
                        <img src="assets/image/thumbs/mini-supra-day-protector.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra day protector</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
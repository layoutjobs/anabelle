<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/lavatorio-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/product-page/condicionador-hidratante.png" alt="condicionador">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">condicionador hidratante</h2>
                        <hr>
                        <p>Desenvolvido com extrato de mandioca e silicones AMINOFUNCIONAL, o condicionador de mandioca treat hair, é rico em proteínas, vitaminas A, C e minerais como ferro, cálcio e fósforo, elementos essenciais para a nutrição da fibra capilar. Condiciona os fios deixando-os macios, sedosos e com brilho intenso.</p>

                        <h5>Volume</h5>
                        <p>5 Litros</p>

                        <h5>Precauções</h5>
                        <p>Manter fora do alcance das crianças. Uso externo. Em caso de contato com os olhos lave com água em abundância. Em caso de irritação suspender o uso, e procure orientação médica.</p>

                        <h5>Modo de Usar</h5>
                        <p>Aplicar o Condicionador Hidratante com Extrato de Mandioca após o uso do Shampoo nos cabelos úmidos e massageando o necessário e enxágue.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua, Parfum, AModimethicone, C11-15 Pareth-7, Cetrimonium Chloride, Cetyl Alcohol, Citric Acid, Cyclomethicoe, Dimethicone, Manihot Utilíssima Leaf Extract, Dissodium EDTA, Glycerin, Isopropyl Palmitate, Laureth-9, Phenyl Trimethicone, Propylene Glycol, Trideceth-12, Treimethylsilylamodimethicone, Paraffinum Liquid.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/black-platinum-progressive-brush">
                        <img src="assets/image/thumbs/black-platinum.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">black platinum</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/mascara-hidratante">
                        <img src="assets/image/thumbs/mascara-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">máscara hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/titanium-colors-caixa">
                        <img src="assets/image/thumbs/mini-titanium-colors.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">titanium colors</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-blond-mascara">
                        <img src="assets/image/thumbs/treat-hair-blond-mascara.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair blond máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-professional-mascara">
                        <img src="assets/image/thumbs/mini-absolut-pro-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair pro máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-blond-shampoo">
                        <img src="assets/image/thumbs/treat-hair-blond-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair blond shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-ativador-de-cachos">
                        <img src="assets/image/thumbs/perfect-waves-ativador-de-cachos.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves ativador de cachos</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/home-care-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/product-page/perfect-waves-shampoo.png" alt="perfect-waves-shampoo">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">perfect waves shampoo</h2>
                        <hr>
                        <p>Um produto com fórmula exclusiva que proporciona limpeza delicada nos cabelos ondulados, encaracolados ou cacheados. Enriquecido com Pantenol (Vitamina B5) e Óleo de abacate (Avocado).</p>

                        <h5>Volume</h5>
                        <p>250ml</p>

                        <h5>Precauções</h5>
                        <p>Mantenha em local fresco ao abrigo de luz, mantenha fora do alcance de crianças, em caso de contato com os olhos, lave abundantemente com água.</p>

                        <h5>Modo de Usar</h5>
                        <p>Aplique o Shampoo Perfect Waves nos cabelos úmidos, massageando por dois minutos. Enxague e repita o processo.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua, sodium laureth sulfate, cocamidopropyl betaine, cocamide dea, ammonium lauryl sulfate, ammonium laureth, 12 sulfate, glycol distearate/sodium laureth sulfate/cocamidopropyl betaime, polyquaternium 7 – polyquaternium, 10/water, sodium cocoamphoacetate, glycol distearate, sodium chloride, parfum:benzyl alcohol/benzyl salicylate/citral/citronellol/geraniol/hexyl cinnamal/hydroxycitronellal/hydroxyisohe, xyl 3, cyclohexene carboxaldehyde/limonene/linalool, panthenol, peg150 distearate, persea gratissima oil, methylchloroisothiazolinone/methylisothiazolinone/magnesium chloride/magnesium nitrate, disodium edta, citric acid.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/black-platinum-progressive-brush">
                        <img src="assets/image/thumbs/black-platinum.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">black platinum</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/shampoo-hidratante">
                        <img src="assets/image/thumbs/shampoo-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">shampoo hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-plus-intensive-mask">
                        <img src="assets/image/thumbs/treat-hair-plus-intensive-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair plus intensive mask</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-plus-capsulas">
                        <img src="assets/image/thumbs/mini-treat-hair-plus-capsula.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair plus cápsulas</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-sinergica-blond">
                        <img src="assets/image/thumbs/mini-supra-blond.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra sinérgica blond</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-sinergico-expert">
                        <img src="assets/image/thumbs/mini-supra.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra sinérgico expert</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/ox-titanium-colors">
                        <img src="assets/image/thumbs/mini-ox-titanium.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">ox titanium colors</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
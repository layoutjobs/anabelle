<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/tratamento-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/product-page/supra-day-protector.png" alt="supra-day-protector">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">supra day protector</h2>
                        <hr>
                        <p>Desenvolvido especialmente para os apaixonados pelos cabelos lisos, o Supra Day Protector é a solução mais inovadora e indispensável para o seu dia a dia, seja você profissional cabeleireiro ou não. Com extrato de chia, estrato de linhaça e óleo de amaranto, o Supra Day Protector oferece diversos benefícios mágicos de uma só vez.</p>

                        <h5>Tamanhos</h5>
                        <p>200ML/floz 6,76</p>

                        <h5>Precauções</h5>
                        <p>Evitar o couro cabeludo. Manter fora do alcance de crianças. Em caso de contato com os olhos, enxaguar abundantemente, havendo algum tipo de irritação procurar orientação médica.</p>

                        <h5>Modo de Usar</h5>
                        <p>Aplique o Supra Day Protector no cabelo limpo e úmido. Por toda extensão da raiz as pontas, seque com o secador e finalize com chapinha para melhores resultados.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>AQUA, DIPROPYLENE GLYCOL/POLYSILICONE-29, PROPYLENE GLYCOL, AMODIMETHICONE/TRIDECETH-12/CETRIMONIUM CHLORIDE, PARFUM:ALPHA-ISOMETHYLIONONE/BUTYLPHENYL MEHTYLPROPIONAL/CINNAMAL/CITRAL/GERANIOL/HEXYLCINNAMAL/HYDROXYCITRONELLAL/HYDROXYSOHEXYL 3-CYCLOHEXENE CARBOXADEHYDE/LIMONENE/LINALOOLLINUM USITATISSIMUM SEED EXTRACT/SALVIA HISPANICA SEED EXTRACT, DISODIUM EDTA, METHYLCHLOROISOTHIAZOLINONE/ METHYLISOTHIAZOLINONE/HELIANTHUS ANNUUS SEED OIL/AMARANTHUS CAUDATUS SEEDEXTRACT/DIISOSTEARYL MALATE/ ROSMARINUS OFFICINALIS LEAF EXTRACT/ CITRIC ACID.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/treat-hair-light-po-descolorante">
                        <img src="assets/image/thumbs/mini-po-descolorante.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">pó descolorante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-professional-condicionador">
                        <img src="assets/image/thumbs/mini-absolut-pro-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut pro condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-condicionador">
                        <img src="assets/image/thumbs/mini-absolut-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-sinergica-expert">
                        <img src="assets/image/thumbs/mini-supra.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra sinérgico expert</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-plus-capsulas">
                        <img src="assets/image/thumbs/mini-treat-hair-plus-capsula.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair plus capsulas</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/mascara-hidratante">
                        <img src="assets/image/thumbs/mascara-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">máscara hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-shampoo">
                        <img src="assets/image/thumbs/perfect-waves-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves shampoo</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
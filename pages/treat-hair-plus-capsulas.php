<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/tratamento-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/product-page/treat-hair-plus-capsula.png" alt="treat-hair-plus-capsulas">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">treat hair plus cápsulas</h2>
                        <hr>
                        <p>Elaborada com partículas dos mais nobres aminoácidos, recupera e trata os fios extremamente danificados tratando o córtex de dentro pra fora. Devolvendo resistência e brilho aos fios. </p>
                        <p>Enriquecido com Sericina, PCA e Arginina, forma uma película em torno do fio mantendo-os mais resistentes e tratados. A Cápsula pode ser diluída ao pó descolorante para tratar o fio.</p>

                        <h5>Quantidade de Cápsulas</h5>
                        <p>30 Cápsulas</p>

                        <h5>Precauções</h5>
                        <p>Manter fora do alcance das crianças. Manter o produto em local fresco, arejado e ao abrigo da luz. Uso externo. Uso profissional.</p>

                        <h5>Modo de Usar</h5>
                        <p>Após lavar os cabelos com o shampoo de pré tratamento, retirar o excesso de água dos fios, misturar em um recipiente plástico o conteúdo de uma ou duas cápsulas a máscara hidratante ou condicionador, misturar  bem, aplicar com auxílio de um pincel em toda extensão dos fios danificados, massagear, deixar agir por 05 a 10 minutos e enxaguar em seguida.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Tetrasodium Disuccinoyl Cystine, Dimethicone, Vinyl Dimethicone Crosspolymer, Silica, Methylparaben, Sodium Pca, Sodium Lactate, Arginine, Aspartic Acid, Pca, Glycine, Alanine, Serine, Valine, Proline, Threonine, Isuleucine Histidine, Phenylalanine, Poliquatemium-7, Sericin, Guar Hydrixypropyltrimonium Chloride, Cetrimonium Chloride, Benentrimonium Chloride, Phenoxyethanol, Sodium Hyaluronate (Hyaluronic Acid), Magnesium Carbonate. </p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/titanium-colors-caixa">
                        <img src="assets/image/thumbs/mini-titanium-colors.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">titanium colors</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-condicionador">
                        <img src="assets/image/thumbs/perfect-waves-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-condicionador">
                        <img src="assets/image/thumbs/mini-absolut-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-liss-pro-shampoo">
                        <img src="assets/image/thumbs/treat-liss-pro-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat liss pro shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-pro-shampoo">
                        <img src="assets/image/thumbs/mini-absolut-pro-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut pro shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-professional-mascara">
                        <img src="assets/image/thumbs/mini-absolut-pro-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut pro mask</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-plus-intensive-mask">
                        <img src="assets/image/thumbs/treat-hair-plus-intensive-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair plus intensive mask</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
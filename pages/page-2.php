<!-- Banner Topo Produtos -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/produtos.png); height:100%;">
            <div class="intro-text uk-padding-large">
                <h1 class="uk-text-center">Inovação e modernização para os fios</h1>
                <p class="uk-text-center">Mude, inove, transforme, use Anabelle</p>
                <hr>
            </div>
        </div>
    </div>
</section>

<!-- Produtos -->
<section id="produtos">
    <div class="uk-section produtos">
        <div class="uk-container">
            <div uk-filter="target: .js-filter">
                <a class="uk-button uk-button-default" href="" uk-icon="chevron-down">Categorias</a>
                <div uk-dropdown="mode: click">
                    <ul class="uk-nav uk-dropdown-nav">
                        <li class="uk-active" uk-filter-control><a href="#">Todos os Produtos</a></li>
                        <li uk-filter-control=".reducao-volume"><a href="#">Redução de Volume</a></li>
                        <li uk-filter-control=".tratamento"><a href="#">Tratamento</a></li>
                        <li uk-filter-control=".coloracao"><a href="#">Coloração</a></li>
                        <li uk-filter-control=".home-care"><a href="#">Home Care</a></li>
                        <li uk-filter-control=".lavatorio"><a href="#">Lavatório</a></li>
                    </ul>
                </div>
                <ul class="js-filter uk-child-width-1-2@s uk-child-width-1-3@m uk-text-center block-products" uk-grid>
                    <li class="tratamento">
                        <div class="uk-card">
                            <a href="produtos/treat-hair-absolut-professional-mascara">Treat Hair Absolut Pro Máscara <img src="assets/image/thumbs/mini-absolut-pro-mask.png" alt="treat-hair-mascara"></a>
                        </div>
                    </li>
                    <li class="coloracao">
                        <div class="uk-card">
                            <a href="produtos/treat-hair-light-po-descolorante">Treat Hair Light - Pó Descolorante <img src="assets/image/thumbs/mini-po-descolorante.png" alt="treat-hair-light-descolorante"></a>
                        </div>
                    </li>
                    <li class="coloracao">
                        <div class="uk-card">
                            <a href="produtos/titanium-colors-caixa">Titanium Colors - Caixa <img src="assets/image/thumbs/mini-titanium-colors.png" alt="titanium-colors"></a>
                        </div>
                    </li>
                    <li class="coloracao">
                        <div class="uk-card">
                            <a href="produtos/ox-titanium-colors">OX Titanium Colors <img src="assets/image/thumbs/mini-ox-titanium.png" alt="ox-titanium"></a>
                        </div>
                    </li>
                    <li class="reducao-volume">
                        <div class="uk-card">
                            <a href="produtos/supra-sinergica-blond">Supra Sinérgica Blond <img src="assets/image/thumbs/mini-supra-blond.png" alt="supra-blond"></a>
                        </div>
                    </li>
                    <li class="reducao-volume">
                        <div class="uk-card">
                            <a href="produtos/black-platinum-progressive-brush">Black Platinum - Progressive Brush <img src="assets/image/thumbs/black-platinum.png" alt="black-platinum"></a>
                        </div>
                    </li>
                    <li class="reducao-volume">
                        <div class="uk-card">
                            <a href="produtos/treat-liss-pro-shampoo">Treat Liss Pro Shampoo <img src="assets/image/thumbs/treat-liss-pro-shampoo.png" alt="treat-liss-pro-shampoo"></a>
                        </div>
                    </li>
                    <li class="reducao-volume">
                        <div class="uk-card">
                            <a href="produtos/treat-liss-pro-redutor-volume">Treat Liss Pro Redutor de Volume <img src="assets/image/thumbs/treat-liss-pro-redutor-de-volume.png" alt="treat-liss-pro-redutor"></a>
                        </div>
                    </li>
                    <li class="tratamento">
                        <div class="uk-card">
                            <a href="produtos/treat-hair-plus-intensive-mask">Treat Hair Plus - Intensive Mask <img src="assets/image/thumbs/treat-hair-plus-intensive-mask.png" alt="treat-hair-plus-intensive-mask"></a>
                        </div>
                    </li>
                </ul>
            </div>
            <br> <br>
            <ul class="uk-pagination uk-flex-center" uk-margin>
                <li class="border-pagination"><a href="#"><span>pag 2 de 3</span></a></li>
                <li><a href="produtos">1</a></li>
                <li><a href="produtos/2">2</a></li>
                <li><a href="produtos/3">3</a></li>
                <li><a href="#"><span uk-pagination-next></span></a></li>
            </ul>
        </div>
    </div>
</section> 
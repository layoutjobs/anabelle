<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/reducao-de-volume-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card uk-text-center">
                        <img src="assets/image/product-page/supra-sinergico-blond.png" alt="supra-sinergico-blond-not-image">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">supra sinérgica blond</h2>
                        <hr>
                        <p>Produto inovador com sua fórmula exclusiva composta por aminoácidos nobres, ácido láctico e pigmentos violeta. Age na fibra capilar desligando temporariamente as pontes de enxofre sem agressão aos fios, depositando aminoácidos, evitando o amarelamento dos fios loiros ou descoloridos. Finalizando com um lindo resultado de cabelo liso e bem tratado em um único produto.</p>

                        <h5>Volume</h5>
                        <p>1 Litro</p>

                        <h5>Precauções</h5>
                        <p>Mantenha em local seco protegido da luz e calor, fora do alcance de crianças. Produto para uso externo. Uso exclusivo profissional. Não aplique o produto se o couro cabeludo estiver irritado ou lesionado. </p>

                        <h5>Modo de Usar</h5>
                        <p>Dividir o cabelo em quadrantes, com o auxílio  de um pincel aplicar o restaurador Supra Sinérgica Blond no cabelo seco, deixar agir de 50 minutos a 1 hora e 20 minutos conforme o objetivo desejado. Enxaguar apenas com água, retirando o excesso do produto, em seguida escovar e pranchar.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua, Silk Amino Acids/Guar Hydroxypropyltrimonium Chloride, Cetearyl Alcohol, Glycerin, Peg-40/PPG-8, Methlaminopropyl/Hydropropyl, Dimethicone Copolymer, Cetearyl Alcohol/Behentrimonium Methosulfate, Cetrimonium Chloride, Stearamidopropyl Dimethylamine, Water/Hydrolyzed, Collagen, Parfum: Butylphenyl Methylpropional/Benzyl Benzoate/Benzyl, Salicylate/Coumarin/Limonene/Hexyl Cinnamal/Hydroxyisohexyl 3-Butter, Phenyl Trimethicone, Vitis Vinifera Oil, Cetylacetate/Acetylated Lanolin Alcohol, Phosphoric Acid, Hydroxyethylcellulose, Disodium Edta, Methylchloroisothiazolinone/Methylisothiazolinone, Acid Violet 43.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/moringa-oil">
                        <img src="assets/image/thumbs/moringa-oil.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">moringa oil</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-plus-intensive-mask">
                        <img src="assets/image/thumbs/treat-hair-plus-intensive-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair plus intensive mask</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-condicionador">
                        <img src="assets/image/thumbs/mini-absolut-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-light-po-descolorante">
                        <img src="assets/image/thumbs/mini-po-descolorante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">pó descolarante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/perfect-waves-condicionador">
                        <img src="assets/image/thumbs/perfect-waves-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">perfect waves condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/shampoo-hidratante">
                        <img src="assets/image/thumbs/shampoo-hidratante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">shampoo hidratante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-blond-mascara">
                        <img src="assets/image/thumbs/treat-hair-blond-mascara.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair blond máscara</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/coloracao-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/product-page/ox-titanium.png" alt="ox-titanium">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">ox titanium colors</h2>
                        <hr>
                        <p>Formulado com Peróxido de Hidrogênio, Lanolina e Macadâmia, podendo ser diluído ao descolorante e nas demais colorações profissionais.</p>

                        <h5>Voulme</h5>
                        <p>900ml</p>

                        <h5>Precauções</h5>
                        <p><strong>CUIDADO:</strong> Contém substâncias passíveis de causar irritação na pele de determinadas pessoas. Antes de usar, faça a prova de toque. Pode causar reação alérgica. Não usar nos cílios e sobrancelhas. Não aplicar se o couro cabeludo estiver irritado ou lesionado. Evitar contato com os olhos. Em caso de contato com os olhos, lavar com água em abundância. Usar luvas adequadas. Contém Peroxido de Hidrogênio. Manter fora do alcance das crianças. Preparado, somente deve ser usado para o fim a que se destina, sendo PERIGOSO para qualquer outro uso. </p>

                        <h5>Prova de Toque</h5>
                        <p>Prepare um pouco deste produto como se fosse utilizá-lo. Aplique no antebraço ou atrás da orelha. Deixe agir por 30 minutos. Lave o local. Aguarde 24 horas, e se neste período surgir irritação na pele , coceira ou ardência no local ou na proximidade, fica comprovada a hipersensibilidade da pessoa ao produto e portanto, o produto não deve ser utilizado.</p>

                        <h5>Modo de Usar</h5>
                        <p>O Creme Revelador deve ser utilizado na preparação de colorações e descolorações capilares na quantidade indicada no modo de preparo do produto a ser utilizado.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua, Hydrogen Peroxide, Cetearyl, Alcohol, Glycerin, Ceteareth-20, Glyceryl Stearate SE, Paraffinum Liquidum, Propylene Glycol, Polysorbate 80, Steareth-21, Parfun Hydroxy-Methylpentylcyclohexenecarboxaldehyde, Hexyl Cinnam-Aldehyde, D-Limonene, Alpha-Isomethyl Ionone, Pentasodium Penteate, Lauramine Oxide, Lanolin, Macadamia Ternifolia Seed Oil, Etidronic Acid, Sodium</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/perfect-waves-condicionador">
                        <img src="assets/image/thumbs/perfect-waves-condicionador.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">perfect waves condicionador</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/moringa-oil">
                        <img src="assets/image/thumbs/moringa-oil.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">moringa oil</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-liss-pro-shampoo">
                        <img src="assets/image/thumbs/treat-liss-pro-shampoo.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat liss pro shampoo</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-sinergica-blond">
                        <img src="assets/image/thumbs/mini-supra-blond.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra sinérgico blond</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-blond-mascara">
                        <img src="assets/image/thumbs/treat-hair-blond-mascara.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair blond máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-light-po-descolorante">
                        <img src="assets/image/thumbs/mini-po-descolorante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">pó descolorante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-condicionador">
                        <img src="assets/image/thumbs/mini-absolut-condicionador.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut condicionador</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 
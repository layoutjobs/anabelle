<!-- Banner Topo -->
<section id="banner-topo-produtos">
    <div>
        <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(assets/image/banners/home-care-intern.png); height: 250px;">
            <div class="intro-text">
                <h1>Produtos</h1>
                <hr>
            </div>
        </div>
    </div>
</section>

<section id="descricao">
    <div class="uk-section product-description">
        <div class="uk-container">
            <div class="uk-grid-small uk-child-width-expand@l" uk-grid>
                <div>
                    <div class="uk-card">
                        <img src="assets/image/product-page/absolut-condicionador.png" alt="absolut-condicionador">
                    </div>
                </div>
                <div>
                    <div class="uk-card description-product">
                        <h2 class="uk-text-left">treat hair absolut condicionador</h2>
                        <hr>
                        <p>Com uma fórmula única rica em polímero hidrantes, condiciona e hidrata o cabelo, deixando o macio e fácil de pentear.</p>

                        <h5>Volume</h5>
                        <p>250ml | 8,45 fl Oz</p>

                        <h5>Precauções</h5>
                        <p>Mantenha em local fresco ao abrigo de luz, mantenha fora do alcance de criança, em caso de contato com os olhos, lave abundantemente com água.</p>

                        <h5>Modo de Usar</h5>
                        <p>Aplique o produto em quantidade suficiente com os cabelos já lavados massageie de raiz para as pontas, deixe agir por alguns minutos, enxague em seguida.</p>
                    </div>
                </div>
            </div>

            <div class="block-produtos-footer">
                <h5>Composição</h5>
                <p>Aqua - Cetearyl alcohol – Glycerin - Cetearyl Alcohol / Behentrimonium Methosulfate - peg-40 / PPG-8 Methylaminopropyl/Hydropropyl Dimethicone Copolymer – Polyquaternium 7 - Cetrimonium Chloride, Behentrimonium Chloride, Stearamidopropyl Dimethylamine, Cl19140 – Parfum : Butylphenyl Methylpropional/Citral/Hexyl Cinnamal/ Limonene/Linalool/Mthyl 2 – Octynoate. Latic Acid – cyclomethicone/dimethicone – butyrospermum park ii (shea) butter – hydroxyethylcellulose – ci 15985 – dissodium edta – methylchloroisothiazolinone / methylisothiazolinone / magnesium chloride / magnesium nitrate – bht.</p>
                <br>
                <h2 class="uk-text-center">Outros Produtos</h2>
                <hr>
            </div>

            <!-- OwlCarousel -->
            <div class="owl-carousel owl-theme">
                <div class="item">
                    <a href="produtos/black-platinum-progressive-brush">
                        <img src="assets/image/thumbs/black-platinum.png" alt="pic-carousel-1">
                        <h6 class="uk-text-center">black platinum</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-light-po-descolorante">
                        <img src="assets/image/thumbs/mini-po-descolorante.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">pó descolorante</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-leave-in">
                        <img src="assets/image/thumbs/mini-absolut-leave-in.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair abosolut leave-in</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-day-protector">
                        <img src="assets/image/thumbs/mini-supra-day-protector.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra day protector</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/treat-hair-absolut-mascara">
                        <img src="assets/image/thumbs/mini-absolut-mask.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">treat hair absolut máscara</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/supra-sinergica-blond">
                        <img src="assets/image/thumbs/mini-supra-blond.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">supra sinérgico blond</h6>
                    </a>
                </div>

                <div class="item">
                    <a href="produtos/ox-titanium-colors">
                        <img src="assets/image/thumbs/mini-ox-titanium.png" alt="pic-carousel-2">
                        <h6 class="uk-text-center">ox titanium colors</h6>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section> 